
<%-- 
    Document   : Home
    Created on : Sep 8, 2023, 10:57:13 PM
    Author     : ADMIN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Image Slider</title>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/style.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

    </head>
    <body>

        <%@include file="Header.jsp" %>
        <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <c:forEach items="${requestScope.sli}" var="sli">
                    <c:if test="${sli.status == true}">
                <div class="carousel-item ${sli.sliderId == 1 ? 'active' : ''}">
                    <a href="${sli.backlink}">
                        <img src="images/${sli.image}" class="d-block w-100" alt="image ${sli.sliderId}">
                    </a>
                    <div class="carousel-caption d-none d-md-block">
                        <h5>${sli.title}</h5>
                        <p>${sli.notes}</p>
                    </div>
                </div>
                    </c:if>
                </c:forEach>
            </div>
            <button class="carousel-control-prev" type="button" data-target="#carouselExampleCaptions" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-target="#carouselExampleCaptions" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </button>
        </div>


        <h6 class="text-heading">Service</h6>  
        <div class="container">
            <!--Slider container-->
            <div id="serviceSlider" class="carousel slide" data-ride="carousel">
                <!--Slides-->
                <div class="carousel-inner">
                    <c:forEach items="${requestScope.ser}" var="s" varStatus="loop">
                        <c:if test="${loop.index % 4 == 0}">
                            <div class="carousel-item ${loop.index == 0 ? 'active' : ''}">
                                <div class="row">
                                </c:if>
                                <div class="col-sm-3">
                                    <a href="detailservice?id=${s.serviceId}">
                                        <img class="service-image" src="images/${s.image}" alt="Image" />
                                    </a>
                                    <h4>${s.serviceName}</h4>
                                </div>
                                <c:if test="${loop.index % 4 == 3 || loop.index == requestScope.ser.size() - 1}">
                                </div>
                            </div>
                        </c:if>
                    </c:forEach>
                </div> 
                <a class="carousel-control-prev" href="#serviceSlider" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#serviceSlider" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>


        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <a href="servicelist">
                        <button class="btn btn-primary">See Details</button>
                    </a>
                </div>
            </div>
        </div>


        <h6 class="text-heading">Post</h6>

        <div class="service-container">
            <div class="row">
                <c:forEach items="${requestScope.po}" var="p">
                    <div class="service-item col-sm-3">
                        <a href="detailpost?pid=${p.postId}">
                            <img class="service-image" src="${p.image}" alt="Image" />
                        </a>
                        <h4>${p.content}</h4>
                    </div>
                </c:forEach>
            </div>
        </div>

        <%@include file="footer.jsp" %>

    </body>
</html>