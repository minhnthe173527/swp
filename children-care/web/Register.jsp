<%-- 
    Document   : register
    Created on : Sep 16, 2023, 4:04:41 PM
    Author     : Lenovo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sign up account</title>
        <style>
            html{
                
                /*                background-image: url('https://i.pinimg.com/564x/1e/c4/ef/1ec4ef1a89291ed80b8534162bd98b22.jpg');*/
                background: linear-gradient(14deg, rgba(2,0,36, 0.8) 0%, rgba(24, 24, 65, 0.7) 66%,
                    rgb(20, 76, 99) 100%), radial-gradient(circle, rgba(2,0,36, 0.5) 0%,
                    rgba(32, 15, 53, 0.2) 65%, rgba(14, 29, 28, 0.9) 100%);
                background-size: cover;
                background-repeat: no-repeat;
                background-position: center;
                background-attachment: fixed;
                height: 100%;
                margin: 0;
            }
            body {
                display: flex;
                justify-content: center;
                align-items: center;
                min-height: 100vh;
                margin: 0;
            }
            .content-left {
                width: 40%;
                padding: 20px;
                text-align: center;
            }
            .signup-form {
                width: 450px;
                height: 400px;
                border: 2px solid #F9D6D6;
                padding-left: 40px;
                padding-bottom:30px;
                border-radius: 5px;
                background-color: white;
            }
            form{
                margin-bottom: 10px;
            }
            .short{
                width: 38.8%;
            }

            input{
                height:30px;
                width : 80%;
                margin-bottom: 10px;
            }
            select{
                height:30px;
                width:20%;
                margin-bottom: 10px;
            }
            input[type="tel"]{
                width:59.2%;
            }
            button{
                background-color: red;
            }
        </style>
    </head>
    <body>  
        <div class="content-left">
            <h1><a href="listservice">Children Care Service</a></h1>
            <p>Ensuring the Well-Being of Your Children</p>
            <p>Sign up an account</p>
        </div>
        <div class="signup-form">
            <h4>Sign up</h4>
            <form action="signup" method="post">
                <c:if test="${msg!=null}">
                    <p style="color:red">${msg}</p>
                </c:if>
                <input class="short" type="text" name="fname" value ="${fname}" placeholder="First Name" pattern="^[A-Za-z]+$" required>
                <input class="short" type="text" name="lname" value ="${lname}" placeholder="Last Name" pattern="^[A-Za-z]+$" required><br>
                <input type="email" name="email" value="${account.email}" placeholder="Email" required><br>
                <input type="password" name="password" value="" placeholder="Password" pattern="^(?=.*[A-Za-z])(?=.*\d).{8,20}$"
                       title="Passwords must have at least 8 characters and contain at least one uppercase letters, lowercase letters, numbers, or symbols!" required><br>
                <select name="gender" required>
                    <option disabled selected value="">Gender</option>
                    <option value="male">Male</option>
                    <option value="female">Female</option>
                </select>
                <input type="tel" name="mobile" value ="${mobile}" placeholder="Phone number" pattern="[0-9]{10}" required title="Please enter a 10-digit phone number (e.g., 0123456789)" required><br>
                <input type="text" name="address" value ="${address}" placeholder="Address" required><br>
                <button type="submit" value="Signup" name="signup" >Sign up</button><br>
            </form>
            <a href="login">Back to login</a><br>
        </div>

    </body>
</html>
