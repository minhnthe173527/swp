<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Login Page</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        html {
/*            background-image: url('https://i.pinimg.com/564x/1e/c4/ef/1ec4ef1a89291ed80b8534162bd98b22.jpg');*/
            background: linear-gradient(14deg, rgba(2,0,36, 0.8) 0%, rgba(24, 24, 65, 0.7) 66%, 
            rgb(20, 76, 99) 100%), radial-gradient(circle, rgba(2,0,36, 0.5) 0%, 
            rgba(32, 15, 53, 0.2) 65%, rgba(14, 29, 28, 0.9) 100%);
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
            background-attachment: fixed;
            height: 100%;
            margin: 0;
        }

        body {
            display: flex;
            justify-content: center;
            align-items: center;
            min-height: 100vh;
            margin: 0;
        }

        .content-left {
            width: 40%; 
            padding: 20px;
            text-align: center;
        }

        .login-form {
            width: 260px;
            height: 400px;
            border: 2px solid #F9D6D6;
            padding-left: 20px;
            padding-right: 20px;
            border-radius: 5px;
            background-color: white;
        }

        .login-form form {
            padding: 20px;
            text-align: center;
        }

        input {
            margin-bottom: 20px;
            text-align: center;
        }

        h4 {
            color: #BBBBBB;
            padding-bottom: 20px;
        }

        ::-webkit-input-placeholder {
            text-align: center;
        }

        :-moz-placeholder {
            text-align: center;
        }

        a {
            padding-bottom: 20px;
            margin-left: 45px;
        }

        button {
            background-color: red;
        }
    </style>
</head>

<body>
    <div class="content-left">
        <h1><a href="home">Children Care Service</a></h1>
        <p>Ensuring the Well-Being of Your Children</p>
        <p>Login to see more details</p>
    </div>
    <div class="login-form">
        <form action="login" method="post">
            <h4>Login</h4>
            <c:if test="${msg!=null}">
                <p class="msg" style="color:red">${msg}</p>
            </c:if>
            <input type="email" name="email" value="${account.email}" placeholder="Email" required><br><!-- comment -->
            <input type="password" name="password" value="" placeholder="Password" required><br>
            <button type="submit" value="Login" name="login">Log in</button><br>
        </form>
        <a href="resetreq">Forgot password?</a><br>
        <a href="signup">Don't have an account?</a>
    </div>
</body>
</html>
