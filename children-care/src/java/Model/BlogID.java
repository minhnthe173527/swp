/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;



/**
 *
 * @author ADMIN
 */

public class BlogID {
    private int cId;
    private int cName;

    public BlogID() {
    }

    public BlogID(int cId, int cName) {
        this.cId = cId;
        this.cName = cName;
    }

    public int getcId() {
        return cId;
    }

    public void setcId(int cId) {
        this.cId = cId;
    }

    public int getcName() {
        return cName;
    }

    public void setcName(int cName) {
        this.cName = cName;
    }
    
}
