<%-- 
    Document   : Blog
    Created on : Sep 21, 2023, 2:22:30 PM
    Author     : hocsi
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">

        <title>Blog</title>

        <!-- Bootstrap core CSS -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">


        <!-- Additional CSS Files -->
        <link rel="stylesheet" href="assets/css/fontawesome.css">
        <link rel="stylesheet" href="assets/css/templatemo-stand-blog.css">
        <link rel="stylesheet" href="assets/css/owl.css">
    </head>
    <body>
        <%@include file="Header.jsp" %>
        <section class="blog-posts grid-system">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="all-blog-posts">
                            <div class="row">

                                <c:forEach items="${blog}" var="b">   
                                    <div class="col-lg-6">
                                        <div class="blog-post">
                                            <div class="blog-thumb">
                                                <a href="blogdetail?bid=${b.getBlogId()}">   <img src="${b.getPost().getImage()}" alt=""></a>
                                            </div>
                                            <div class="down-content">
                                                <span>${b.getCategory().getName()}</span>
                                                <a href="blogdetail?bid=${b.getBlogId()}"><h4>${b.getTitle()}</h4></a>
                                                <ul class="post-info">
                                                    <li><a href="blogdetail?bid=${b.getBlogId()}">${b.getAuthor().getFullname()}</a></li>
                                                    <li><a href="blogdetail?bid=${b.getBlogId()}">${b.getDate()}</a></li>             
                                                </ul>
                                                <p>${b.getContent()}</p>                           
                                            </div>
                                        </div>
                                    </div>
                                </c:forEach>


                                <div class="col-lg-12">
                                    <ul class="page-numbers">
                                        <li><a href="blog?index=1"><i class="fa fa-angle-double-left"></i></a></li>
                                                <c:forEach var = "i" begin = "1" end = "${numberPage}">
                                            <li class="${param['index']==i?'active':''}"><a href="blog?index=${i}"><span>${i}</span></a></li>
                                                    </c:forEach>
                                        <li><a href="blog?index=${numberPage}"><i class="fa fa-angle-double-right"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="sidebar">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="sidebar-item search">
                                        <form id="search_form" name="gs" method="post" action="blog">
                                            <input type="text" name="search" class="searchText" placeholder="type to search..." autocomplete="on">
                                        </form>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="sidebar-item recent-posts">
                                        <div class="sidebar-heading">
                                            <h2>Recent Posts</h2>
                                        </div>
                                        <div class="content">
                                            <ul>
                                                <c:forEach var="r" items="${recent}">
                                                    <li><a href="blogdetail?bid=${r.getBlogId()}">
                                                            <h5>${r.getTitle()}</h5>
                                                            <span>${r.getDate()}</span>
                                                        </a></li>
                                                    </c:forEach>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <ul class="social-icons">
                            <li><a href="#">Facebook</a></li>
                            <li><a href="#">Twitter</a></li>
                            <li><a href="#">Behance</a></li>
                            <li><a href="#">Linkedin</a></li>
                            <li><a href="#">Dribbble</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>


        <!-- Bootstrap core JavaScript -->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Additional Scripts -->
        <script src="assets/js/custom.js"></script>
        <script src="assets/js/owl.js"></script>
        <script src="assets/js/slick.js"></script>
        <script src="assets/js/isotope.js"></script>
        <script src="assets/js/accordions.js"></script>

    </body>
</html>

