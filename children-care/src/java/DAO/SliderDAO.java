/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;

import Model.Slider;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.input.KeyCode;

/**
 *
 * @author minh1
 */
public class SliderDAO extends DBContext {

    public List<Slider> getAllSlider() {
        List<Slider> ls = new ArrayList<>();
        String sql = "SELECT * FROM [SWP].[dbo].[Slider]";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Slider s = new Slider();
                s.setSliderId(rs.getInt("sliderId"));
                s.setTitle(rs.getString("title"));
                s.setImage(rs.getString("image"));
                s.setBacklink(rs.getString("backlink"));
                s.setNotes(rs.getString("notes"));
                s.setStatus(rs.getBoolean("status"));
                ls.add(s);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return ls;
    }

    public void deleteSlider(String id) {
        String sql = "Delete from [dbo].[Slider]\n"
                + "     where SliderId =? ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, id);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void updateSlider(Slider s) {
        String sql = "UPDATE [dbo].[Slider]\n"
                + "   SET [Title] = ?\n"
                + "      ,[Image] = ?\n"
                + "      ,[Backlink] = ?\n"
                + "      ,[Notes] = ?\n"
                + "      ,[Status] = ?\n"
                + " WHERE [SliderId]=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, s.getTitle());
            st.setString(2, s.getImage());
            st.setString(3, s.getBacklink());
            st.setString(4, s.getNotes());
            st.setBoolean(5, s.isStatus());
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void updateStatusSlider(int sliderId, boolean newStatus) {
        String sql = "UPDATE [dbo].[Slider]\n"
                + "  SET [Status] = ?\n"
                + " WHERE [SliderId] = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setBoolean(1, newStatus);
            st.setInt(2, sliderId);
            int cc = st.executeUpdate();
            if (cc > 0) {
                System.out.println("done");
            } else {
                System.out.println("ngu");
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public boolean getStatusById(int sliderId) {
        String sql = "SELECT [Status] FROM [dbo].[Slider] WHERE [SliderId] = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, sliderId);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getBoolean("Status");
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return false;
    }

    public Slider getSliderById(String id) {
        String sql = "SELECT * FROM [dbo].[Slider] WHERE [SliderId] = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Slider s = new Slider();
                s.setSliderId(rs.getInt("sliderId"));
                s.setTitle(rs.getString("title"));
                s.setImage(rs.getString("image"));
                s.setBacklink(rs.getString("backlink"));
                s.setNotes(rs.getString("notes"));
                s.setStatus(rs.getBoolean("status"));
                return s;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public List<Slider> searchSliderByName(String name) {
        ArrayList<Slider> ls = new ArrayList<>();
        String sql = "SELECT [SliderId]\n"
                + "      ,[Title]\n"
                + "      ,[Image]\n"
                + "      ,[Backlink]\n"
                + "      ,[Notes]\n"
                + "      ,[Status]\n"
                + "  FROM [SWP].[dbo].[Slider] where Title like ? or Backlink like ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%"+name+"%");
            st.setString(2, "%"+name+"%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Slider s = new Slider();
                s.setSliderId(rs.getInt("sliderId"));
                s.setTitle(rs.getString("title"));
                s.setImage(rs.getString("image"));
                s.setBacklink(rs.getString("backlink"));
                s.setNotes(rs.getString("notes"));
                s.setStatus(rs.getBoolean("status"));
                ls.add(s);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return ls;
    }

    public List<Slider> searchSliderByStatus(boolean status) {
        ArrayList<Slider> ls = new ArrayList<>();
        String sql = "SELECT [SliderId]\n"
                + "      ,[Title]\n"
                + "      ,[Image]\n"
                + "      ,[Backlink]\n"
                + "      ,[Notes]\n"
                + "      ,[Status]\n"
                + "  FROM [SWP].[dbo].[Slider] where Status like ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setBoolean(1, status);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Slider s = new Slider();
                s.setSliderId(rs.getInt("sliderId"));
                s.setTitle(rs.getString("title"));
                s.setImage(rs.getString("image"));
                s.setBacklink(rs.getString("backlink"));
                s.setNotes(rs.getString("notes"));
                s.setStatus(rs.getBoolean("status"));
                ls.add(s);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return ls;
    }

    public static void main(String[] args) {
        SliderDAO sd = new SliderDAO();
        
        List<Slider> ls = sd.searchSliderByStatus(true);
        for (Slider l : ls) {
            System.out.println(l.getImage());
        }
        
    }
}
