/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package Controllers.Public;

import DAO.ServiceCategoryDAO;
import DAO.ReservationDao;
import Model.Account;
import Model.Cart;
import Model.ServiceCategory;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author DELL
 */
@WebServlet(name="ReservationContactController", urlPatterns={"/ReservationContact"})
public class ReservationContactController extends HttpServlet {
  
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
         HttpSession session = request.getSession();

        Account Cus = (Account) session.getAttribute("account");
        int CusID = Cus.getId();
        String ServiceIDstr[] = request.getParameterValues("ServiceID");
        if(ServiceIDstr==null){
            response.sendRedirect("ReservationDetails");
        }else{
        int ServiceID[] = new int[ServiceIDstr.length];
        for (int i = 0; i < ServiceID.length; i++) {
            ServiceID[i] = Integer.parseInt(ServiceIDstr[i]);
        }
        ReservationDao crd = new ReservationDao();
        List<Cart> lcrn = new ArrayList<>();
        List<Cart> lcr = crd.getCartByCusID(CusID);
        for (int i = 0; i < ServiceID.length; i++) {
            for (Cart olcr : lcr) {
                if (olcr.getService().getServiceId() == ServiceID[i]) {
                    lcrn.add(olcr);
                }
            }
        }
        double total = 0;
        for (Cart cart : lcrn) {
            total = total + (cart.getService().getPrice() * cart.getQuantity()*cart.getNumberPerson());
        }
        request.setAttribute("total", total);
        request.setAttribute("listCart", lcrn);
        ServiceCategoryDAO catedao = new ServiceCategoryDAO();
        List<ServiceCategory> listcategory = catedao.getAllServiceCategory();
        request.setAttribute("listcategory", listcategory);
        request.getRequestDispatcher("ReservationContact.jsp").forward(request, response);}
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
