/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

/**
 *
 * @author SANG
 */
public class Cart {
//    serviceID int references Service(ServiceID),
//	customerID int references CustomerAccount(CustomerID),
//	quantity int,
//	numberPerson int
    
    Service service;
    int customerID;
    int quantity;
    int numberPerson;

    public Cart() {
    }

    public Cart(Service service, int customerID, int quantity, int numberPerson) {
        this.service = service;
        this.customerID = customerID;
        this.quantity = quantity;
        this.numberPerson = numberPerson;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getNumberPerson() {
        return numberPerson;
    }

    public void setNumberPerson(int numberPerson) {
        this.numberPerson = numberPerson;
    }

    
}
