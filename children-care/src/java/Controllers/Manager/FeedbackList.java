/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package Controllers.Manager;

import DAO.FeedBackDAO;
import Model.Feedback;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;

/**
 *
 * @author SANG
 */
public class FeedbackList extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet FeedbackList</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet FeedbackList at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String star = request.getParameter("star") == null ? "" : request.getParameter("star");
        String status = request.getParameter("status") == null ? "" : request.getParameter("status");
        String searchfeedbackcontent = request.getParameter("searchfeedbackcontent") == null ? "" : request.getParameter("searchfeedbackcontent");
        String index = request.getParameter("index") == null ? "1" : request.getParameter("index");
        String searchfullname= request.getParameter("searchfullname") == null ? "" : request.getParameter("searchfullname");
        String searchservice= request.getParameter("searchservice") == null ? "" : request.getParameter("searchservice");
        FeedBackDAO  dao = new FeedBackDAO();
        int total = dao.getAllFeedbackByCondtion(status, star, searchfeedbackcontent, searchfullname, searchservice, 1, 9999999).size();
        int numberPage = (int) Math.ceil((double) total / 6);
        List<Feedback> fl = dao.getAllFeedbackByCondtion(status, star, searchfeedbackcontent, searchfullname, searchservice, Integer.valueOf(index), 6);
        request.setAttribute("feedbacksList", fl);
        request.setAttribute("numberPage", numberPage);
//        for (Feedback feedback : fl) {
//            response.getWriter().print(feedback.getFeedback());
//        }
        request.getRequestDispatcher("./marketing/FeedbacksList.jsp").forward(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
