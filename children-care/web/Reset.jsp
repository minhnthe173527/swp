<%-- 
    Document   : getResetEmail
    Created on : Sep 17, 2023, 11:44:55 AM
    Author     : Lenovo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Reset Page</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            html{
                background-image: url('');
                background-size: cover;
                background-repeat: no-repeat;
                background-position: center;
                background-attachment: fixed;
                height: 100%;
                margin: 0;
            }
            body {
                display: flex;
                justify-content: center;
                align-items: center;
                min-height: 100vh;
                margin: 0;
            }

            .reset-form {
                width: 200px;
                height: 180px;
                border: 2px solid #F9D6D6;
                padding-left: 20px;
                padding-right: 20px;
                border-radius: 5px;
            }

            input{
                margin-bottom: 10px;
            }


            h4{
                color:#BBBBBB;
            }


            a{
                padding-bottom: 20px;
                
            }
            button{
                background-color:red;
            }

        </style>
    </head>
    <body>
        <div class="reset-form">            
            <form action="resetreq" method="post">    
                <h4>Reset Password</h4>
                <c:if test="${msg!=null}">
                    <p style="color:red">${msg}</p>
                </c:if>
                <input type="email" name="email" value="${account.email}" placeholder="Email" required><br><!-- comment -->
                <button type="submit" value="Request" name="request" >Request</button><br>

                <a href="login">Back to login</a><br>
            </form>
        </div>
    </body>
</html>
