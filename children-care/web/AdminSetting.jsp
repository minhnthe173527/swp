<%-- 
   Document   : ManageServiceDetail
   Created on : Oct 14, 2023, 2:52:00 PM
   Author     : ADMIN
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Admin Dashboard</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Boxicons -->
        <link href="./marketing/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="./marketing/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="./marketing/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link href="./marketing/css/morris/morris.css" rel="stylesheet" type="text/css" />
        <link href="./marketing/css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
        <link href="./marketing/css/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
        <link href="./marketing/css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
        <link href="./marketing/css/iCheck/all.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="./marketing/css/style.css" rel="stylesheet" type="text/css" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>

        <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/style.css">

        <script src="ckeditor/ckeditor.js"></script> 
        <script src="ckfinder/ckfinder.js"></script>
        <style>
            *, a{
                font-size: large;
                text-decoration: none;
            }

            .dropdown {
                position: relative;
                display: inline-block;
            }

            .dropdown-content {
                display: none;
                position: absolute;
                background-color: #f9f9f9;
                min-width: 120px;
                box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
                z-index: 1;
            }

            .dropdown:hover .dropdown-content {
                display: block;
            }

            .dropdown-content a {
                color: black;
                padding: 12px 16px;
                text-decoration: none;
                display: block;
            }
        </style>
    </head>
    <body class="skin-black">
        <!-- header logo: style can be found in header.less -->
        <div class="left-side sidebar-offcanvas"  style="height: 1200px;position: fixed; width: 220px; background-color: #39435c;left: 0; top:0;">
        </div>
        <header class="header">
            <a href="" class="logo">
                Admin
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <div></div>
                <div></div>
                <div class="navbar-right">
                    <a href="logout" class="btn btn-outline-primary" style="text-decoration: none;  font-weight: 100;" >
                        <i class="fa fa-sign-out" style="font-size: 35px;" aria-hidden="true"></i>
                    </a>
                </div>
            </nav>
        </header>
        <!-- SIDEBAR -->
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas" id="left-aside" style="height: 100%;">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <c:if test="${sessionScope.u.getAvartar()==null}">
                                <img src="https://dvdn247.net/wp-content/uploads/2020/07/avatar-mac-dinh-1.png" class="img-circle" alt="User Image" />
                            </c:if>
                            <c:if test="${sessionScope.u.getAvartar()!=null}">
                                <img src="${sessionScope.u.getAvartar()}" class="img-circle" alt="User Image" />
                            </c:if>
                        </div>
                        <div class="pull-left info">
                            <p>Hello, ${sessionScope.account.fullname}</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu"> 
                        
                        <li class="">
                            <a href="AdminSetting">
                                <i class="fa fa-circle"></i> <span>Setting List</span>
                            </a>
                        </li>
                       
                        <li class="">
                            <a href="ManageAccount">
                                <i class="fa fa-circle"></i> <span>Manager Account</span>
                            </a>
                        </li>
                        
                        <li class="">
                            <a href="home">
                                <i class="fa fa-circle"></i> <span>Go Home</span>
                            </a>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <h2 style="text-align: center">Setting List</h2>

            <div class="container" style="margin-bottom: 50vh">
                <div class="layout-specing">
                    <form action="AdminSearchSetting" role="search" method="get" id="searchform" class="searchform w-25 d-flex">

                        <input type="text" class="form-control rounded-pill" name="search" id="s" placeholder="Search Keywords..." value="${searchValue}">
                        <input type="submit" id="searchsubmit" value="Search">

                    </form>
                    <div class="d-md-flex justify-content-between">
                        <nav aria-label="breadcrumb" class="d-inline-block mt-4 mt-sm-0">
                            <ul class="breadcrumb bg-transparent rounded mb-0 p-0">
                                <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Settings</li>
                            </ul>
                        </nav>
                        <div class="col-xl-3 col-md-6 mt-4 mt-md-0 text-md-end ">
                            <a href="AdminAddNewSetting.jsp" class="btn btn-primary fs-2">Add New Setting</a>
                        </div><!--end col-->
                    </div>
                    <div class="d-md-flex justify-content-between mt-4">
                        <div class="mb-0">
                            <span style="font-size: larger;">Filter: </span>
                            <!-- Example single danger button -->

                            <div class="btn-group">
                                <button type="button" class="btn btn-primary dropdown-toggle fs-2" data-bs-toggle="dropdown"
                                        aria-expanded="false" >
                                    <c:if test="${requestScope.status == null}">--All Status--</c:if>
                                    <c:if test="${requestScope.status != null}">${status}</c:if>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a class="dropdown-item" href="AdminSetting">--All Status--</a>
                                        </li>
                                    <c:forEach items="${allStatus}" var="s">
                                        <li><a class="dropdown-item"
                                               href='AdminFilterStatusSetting?status=${s}'>${s}</a></li>
                                        </c:forEach>
                                </ul>
                            </div>
                        </div>
                        <div class="d-md-flex">
                            <span class="mb-0">Sort by:</span>
                            <div class="dropdown " style="margin-left: 10px;">
                                <div class="bg-soft-primary" style="width: 120px; text-align: center;"
                                     onclick="toggleDropdown(this)" id="SortByName">Id</div>
                                <div class="dropdown-content">
                                    <a
                                        href="AdminSortSetting?sortField=SettingID&sortDir=desc">Descending</a>
                                    <div class="dropdown-divider"></div>
                                    <a
                                        href="AdminSortSetting?sortField=SettingID&sortDir=asc">Ascending</a>
                                </div>
                            </div>
                            <div class="dropdown " style="margin-left: 10px;">
                                <div class="bg-soft-primary" style="width: 120px; text-align: center;"
                                     onclick="toggleDropdown(this)" id="SortByName">Name</div>
                                <div class="dropdown-content">
                                    <a
                                        href="AdminSortSetting?sortField=Name&sortDir=desc">Descending</a>
                                    <div class="dropdown-divider"></div>
                                    <a
                                        href="AdminSortSetting?sortField=Name&sortDir=asc">Ascending</a>
                                </div>
                            </div>
                            <div class="dropdown " style="margin-left: 10px;">
                                <div class="bg-soft-primary" style="width: 120px; text-align: center;"
                                     onclick="toggleDropdown(this)" id="SortByName">Value</div>
                                <div class="dropdown-content">
                                    <a
                                        href="AdminSortSetting?sortField=Value&sortDir=desc">Descending</a>
                                    <div class="dropdown-divider"></div>
                                    <a
                                        href="AdminSortSetting?sortField=Value&sortDir=asc">Ascending</a>
                                </div>
                            </div>
                            <div class="dropdown " style="margin-left: 10px;">
                                <div class="bg-soft-primary" style="width: 120px; text-align: center;"
                                     onclick="toggleDropdown(this)" id="SortByPrice">Status</div>
                                <div class="dropdown-content ">
                                    <a
                                        href="AdminSortSetting?sortField=Status&sortDir=desc">Descending</a>
                                    <div class="dropdown-divider"></div>
                                    <a
                                        href="AdminSortSetting?sortField=Status&sortDir=asc">Ascending</a>
                                </div>
                            </div>

                            <script>
                                function toggleDropdown(element) {
                                    const dropdownContent = element.nextElementSibling;
                                    dropdownContent.style.display = dropdownContent.style.display === "block" ? "none" : "block";
                                }
                            </script>
                        </div>
                        <div></div>
                    </div>

                    <div class="row">
                        <div class="col-12 mt-4">
                            <div class="table-responsive shadow rounded">
                                <table class="table table-center bg-white mb-0">
                                    <thead>
                                        <tr>
                                            <th class="border-bottom p-3" style="min-width: 50px;">Id</th>
                                            <th class="border-bottom p-3" style="min-width: 100px;">Name</th>
                                            <th class="border-bottom p-3">Value</th>
                                            <th class="border-bottom p-3" style="min-width: 180px;">Description</th>
                                            <th class="border-bottom p-3" style="width: 50px;">Status</th>
                                            <th class="border-bottom p-3" style="min-width: 100px;"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${listS}" var="o">

                                            <tr>
                                                <th class="p-3">${o.settingId}</th>
                                                <td class="p-3">${o.name}</td>
                                                <td class="p-3">${o.value}</td>
                                                <td class="p-3">${o.description}</td>
                                                <td class="p-3">${o.status}</td>
                                                <td class="text-end p-3">

                                                    <a href="AdminSettingDetail?id=${o.settingId}" class="btn btn-icon btn-pills bg-aqua fs-3">View</a>
                                                    <a href="AdminSettingEdit?id=${o.settingId}" class="btn btn-icon btn-pills bg-aqua fs-3">Edit</a>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                        <c:if test="${requestScope.mess!=null}">
                                            <tr style="padding: 30px">
                                                   <div class="alert alert-success text-center">${mess}</div>
                                            </tr>
                                        </c:if>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div><!--end row-->

                    <div class="row text-center">
                        <!-- PAGINATION START -->
                        <div class="col-12 mt-4">
                            <div class="d-md-flex align-items-center text-center justify-content-between">
                                <span class="text-muted me-3"></span>
                                <ul class="pagination justify-content-center mb-0 mt-3 mt-sm-0">
                                    <c:if test="${requestScope.status == null && requestScope.searchValue == null && requestScope.sortField == null }">
                                        <c:forEach begin="1" end="${endPage}" var="i">
                                            <c:if test="${index==i}">
                                                <li class="page-item active"><a class="page-link fs-3" href="AdminSetting?index=${i}">${i}</a></li>
                                                </c:if>
                                                <c:if test="${index!=i}">
                                                <li class="page-item "><a class="page-link fs-3" href="AdminSetting?index=${i}">${i}</a></li>
                                                </c:if>
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${requestScope.status != null}">
                                            <c:forEach begin="1" end="${endPage}" var="i">
                                                <c:if test="${index==i}">
                                                <li class="page-item active"><a class="page-link fs-3" href="AdminFilterStatusSetting?index=${i}&status=${status}">${i}</a></li>
                                                </c:if>
                                                <c:if test="${index!=i}">
                                                <li class="page-item "><a class="page-link fs-3" href="AdminFilterStatusSetting?index=${i}&status=${status}">${i}</a></li>
                                                </c:if>
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${requestScope.searchValue != null}">
                                            <c:forEach begin="1" end="${endPage}" var="i">
                                                <c:if test="${index==i}">
                                                <li class="page-item active"><a class="page-link fs-3" href="AdminSearchSetting?index=${i}&search=${searchValue}">${i}</a></li>
                                                </c:if>
                                                <c:if test="${index!=i}">
                                                <li class="page-item "><a class="page-link fs-3" href="AdminSearchSetting?index=${i}&search=${searchValue}">${i}</a></li>
                                                </c:if>
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${requestScope.sortField != null}">
                                            <c:forEach begin="1" end="${endPage}" var="i">
                                                <c:if test="${index==i}">
                                                <li class="page-item active"><a class="page-link fs-3" href="AdminSortSetting?index=${i}&sortField=${sortField}&sortDir=${sortDir}">${i}</a></li>
                                                </c:if>
                                                <c:if test="${index!=i}">
                                                <li class="page-item "><a class="page-link fs-3" href="AdminSortSetting?index=${i}&sortField=${sortField}&sortDir=${sortDir}"">${i}</a></li>
                                                </c:if>
                                            </c:forEach>
                                        </c:if>
                                </ul>
                            </div>
                        </div><!--end col-->
                        <!-- PAGINATION END -->
                    </div><!--end row-->
                </div>
            </div><!--end container-->
        </div>

    </body>
</html>
