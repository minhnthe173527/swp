/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controllers.Manager;

import DAO.ManageSeviceDAO;
import DAO.ServiceCategoryDAO;
import DAO.ServiceListDAO;
import Model.ServiceCategory;
import Model.ServiceList;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;

/**
 *
 * @author ADMIN
 */
@WebServlet(name = "ManageServiceList", urlPatterns = {"/manageservicelist"})
public class ManageServiceList extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String indexPage = request.getParameter("page");
        String btnAdd = request.getParameter("btnAdd");
        String filterStatus = request.getParameter("filterStatus");
        String SearchValue = request.getParameter("txtSearchValue");
        String SortField = request.getParameter("sortField");
        ManageSeviceDAO dao = new ManageSeviceDAO();
        List<ServiceList> sv;
        if (btnAdd != null) {
            int svId = Integer.parseInt(request.getParameter("svId"));
            String svName = request.getParameter("svName");
            String svDescription = request.getParameter("svDescription");
            double svPrice = Double.parseDouble(request.getParameter("svPrice"));
            int svCategory = Integer.parseInt(request.getParameter("svCategory"));
            String svImage = "";
            Boolean svStatusRaw = Boolean.parseBoolean(request.getParameter("svStatus"));
            int svStatus;
            if (svStatusRaw == true) {
                svStatus = 1;
            } else {
                svStatus = 0;
            }
            if (dao.isDuplicateService(svId, svName)) {
                request.setAttribute("mess", "Id or Service Name is Duplicate");
                request.getRequestDispatcher("AddNewService.jsp").forward(request, response);
            } else {
                dao.createService(svId, svName, svDescription, svPrice, svCategory, svImage, svStatus);
                response.sendRedirect("manageservicelist");
            }
        } else {
            if (filterStatus != null) {
                if (filterStatus.equalsIgnoreCase("false")) {
                    int filter = 0;
                    boolean change = true;
                    sv = dao.getServicebyStatus(filter);
                    request.setAttribute("listsvp", sv);
                    request.setAttribute("filter", change);
                    request.getRequestDispatcher("ManageService.jsp").forward(request, response);
                } else if (filterStatus.equalsIgnoreCase("true")) {
                    int filter = 1;
                    boolean change = false;
                    sv = dao.getServicebyStatus(filter);
                    request.setAttribute("listsvp", sv);
                    request.setAttribute("filter", change);
                    request.getRequestDispatcher("ManageService.jsp").forward(request, response);
                }
            } else if (SearchValue != null) {
                ServiceListDAO svdao = new ServiceListDAO();
                sv = svdao.SearchService(SearchValue);
                request.setAttribute("listsvp", sv);
                request.getRequestDispatcher("ManageService.jsp").forward(request, response);
            } else if (SortField != null) {
                if(SortField.equals("ServiceName")){
                    sv = dao.getServicesortbyName();
                    request.setAttribute("listsvp", sv);
                }else if(SortField.equals("Category")){
                    sv = dao.getServicesortbyCategoryId();
                    request.setAttribute("listsvp", sv);
                }else if(SortField.equals("Price")){
                    sv = dao.getServicesortbyPrice();
                    request.setAttribute("listsvp", sv);
                }else if(SortField.equals("Status")){
                    sv = dao.getServicesortbyStatus();
                    request.setAttribute("listsvp", sv);
                }
                request.getRequestDispatcher("ManageService.jsp").forward(request, response);
            } else {
                if (indexPage == null) {
                    indexPage = "1";
                }
                int index = Integer.parseInt(indexPage);
                int count;
                int endPage;
                count = dao.getQuantityService();
                endPage = count / 4;
                if (count % 4 != 0) {
                    endPage++;
                }
                Boolean change = false;
                sv = dao.paggingService(index);
                request.setAttribute("endP", endPage);
                request.setAttribute("listsvp", sv);
                request.setAttribute("tag", index);
                request.setAttribute("filter", change);
                request.getRequestDispatcher("ManageService.jsp").forward(request, response);
            }
        }
    }

    public static void main(String[] args) {
        ManageSeviceDAO dao = new ManageSeviceDAO();
        List<ServiceList> sv = dao.getAllService();
        for (ServiceList o : sv) {
            System.out.println(o);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
