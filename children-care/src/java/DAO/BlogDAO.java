/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;


import Model.Blog;
import Model.Category;
import Model.Post;
import Model.Staff;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author asus
 */
public class BlogDAO extends DBContext {

    public ArrayList<Blog> getBlog(int index) {
        ArrayList<Blog> list = new ArrayList<>();
        String sql = "SELECT * FROM [Blog] b\n"
                + "INNER JOIN Post p ON b.PostID = p.PostID\n"
                + "INNER JOIN BlogCategory c ON b.CategoryID = c.cID\n"
                + "INNER JOIN EmployeeAccount s ON b.AuthorID = s.ID\n"
                + "order by b.PublishedDate desc\n"
                + "OFFSET ? ROWS FETCH NEXT 4 ROWS ONLY";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, (index - 1) * 4);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Blog(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getDate(4), new Staff(rs.getString(16)),
                        new Category(rs.getInt(13), rs.getString(14)), new Post(rs.getInt(8), rs.getString(9), rs.getString(10), rs.getString(11), rs.getString(12))));
            }
        } catch (SQLException e) {

        }
        return list;
    }
    
    public static void main(String[] args) {
        BlogDAO dao =new BlogDAO();
        System.out.println(dao.getRecentBlog());
        System.out.println(dao.getBlogByID(1));
    }

    public int getNumberBlog() {
        ArrayList<Blog> list = new ArrayList<>();
        String sql = "SELECT count(*) FROM [Blog] b inner join Post p on b.PostID = p.PostID \n"
                + "                inner join BlogCategory c on b.CategoryID = c.cID inner join EmployeeAccount s on b.AuthorID = s.ID";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException e) {
        }
        return 0;
    }

    public ArrayList<Blog> getRecentBlog() {
        ArrayList<Blog> list = new ArrayList<>();
        String sql = "SELECT * FROM [Blog] b\n"
                + "INNER JOIN Post p ON b.PostID = p.PostID\n"
                + "INNER JOIN BlogCategory c ON b.CategoryID = c.cID\n"
                + "INNER JOIN EmployeeAccount s ON b.AuthorID = s.ID\n"
                + "order by b.PublishedDate desc\n"
                + "OFFSET 4 ROWS FETCH NEXT 5 ROWS ONLY";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Blog(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getDate(4), new Staff(rs.getString(16)),
                        new Category(rs.getInt(13), rs.getString(14)), new Post(rs.getInt(8), rs.getString(9), rs.getString(10), rs.getString(11), rs.getString(12))));
            }

        } catch (SQLException e) {

        }
        return list;
    }

    public Blog getBlogByID(int id) {
        try {
            String sql = "SELECT * FROM [Blog] b inner join Post p on b.PostID = p.PostID \n"
                    + "inner join BlogCategory c on b.CategoryID = c.cID inner join EmployeeAccount s on b.AuthorID = s.ID where b.BlogID = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Blog blog = new Blog(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getDate(4), new Staff(rs.getString(16)),
                        new Category(rs.getInt(13), rs.getString(14)), new Post(rs.getInt(8), rs.getString(9), rs.getString(10), rs.getString(11), rs.getString(12)));
                return blog;
            }

        } catch (SQLException e) {

        }
        return null;
    }

    public ArrayList<Blog> searchBlog(String search) {
        ArrayList<Blog> list = new ArrayList<>();
        String sql = "SELECT * FROM [Blog] b inner join Post p on b.PostID = p.PostID \n"
                + "inner join BlogCategory c on b.CategoryID = c.cID inner join EmployeeAccount s on b.AuthorID = s.ID where b.Title LIKE ? OR c.Cname LIKE ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, "%" + search + "%");
            ps.setString(2, "%" + search + "%");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Blog(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getDate(4), new Staff(rs.getString(16)),
                        new Category(rs.getInt(13), rs.getString(14)), new Post(rs.getInt(8), rs.getString(9), rs.getString(10), rs.getString(11), rs.getString(12))));
            }

        } catch (SQLException e) {

        }
        return list;
    }
}
