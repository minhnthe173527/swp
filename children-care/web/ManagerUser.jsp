
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="./marketing/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="./marketing/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="./marketing/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link href="./marketing/css/morris/morris.css" rel="stylesheet" type="text/css" />
        <link href="./marketing/css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
        <link href="./marketing/css/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
        <link href="./marketing/css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
        <link href="./marketing/css/iCheck/all.css" rel="stylesheet" type="text/css" />
        <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
        <!-- Theme style -->
        <link href="./marketing/css/style.css" rel="stylesheet" type="text/css" />

        <style type="text/css">
            td{
                padding: 5px;
            }
            th{
                padding: 5px;
            }
        </style>
    </head>
    <body class="skin-black">
        <c:if test="${param['index']==null }">   
            <c:set var = "index" scope = "page" value = "1"/>
        </c:if>
        <c:if test="${param['index']!=null}">
            <c:set var = "index" scope = "page" value = "${param['index']}"/>
        </c:if>
        <!-- header logo: style can be found in header.less -->
        <div class="left-side sidebar-offcanvas"  style="height: 1200px;position: fixed; width: 220px; background-color: #39435c;left: 0; top:0;">
        </div>
        <header class="header">
            <a href="" class="logo">
                Manager
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <a href="logout" class="btn btn-outline-primary" style="text-decoration: none;  font-weight: 100;" >
                        <i class="fa fa-sign-out" style="font-size: 35px;" aria-hidden="true"></i>
                    </a>
                </div>
            </nav>
        </header>


        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas" id="left-aside" style="height: 100%;">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <c:if test="${sessionScope.u.getAvartar()==null}">
                                <img src="https://dvdn247.net/wp-content/uploads/2020/07/avatar-mac-dinh-1.png" class="img-circle" alt="User Image" />
                            </c:if>
                            <c:if test="${sessionScope.u.getAvartar()!=null}">
                                <img src="${sessionScope.u.getAvartar()}" class="img-circle" alt="User Image" />
                            </c:if>
                        </div>
                        <div class="pull-left info">
                            <p>Hello, ${sessionScope.u.getFullname()}</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu"> 
                        <li class="">
                            <a href="manageservicelist">
                                <i class="fa fa-circle"></i> <span>Service List</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="FeedbackList">
                                <i class="fa fa-circle"></i> <span>Feedback List</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="managerslider">
                                <i class="fa fa-circle"></i> <span>Sliders List</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="ManagePost">
                                <i class="fa fa-circle"></i> <span>Post List</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="manageruser">
                                <i class="fa fa-circle"></i> <span>Customer List</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="home">
                                <i class="fa fa-circle"></i> <span>Go Home</span>
                            </a>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>
            <aside class="right-side">

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="panel">
                                <header class="panel-heading">
                                    Manager User
                                </header>                
                                <div class="panel-body table-responsive">
                                    <div class="sliderList">
                                        <div class="input-group" style="margin-bottom: 10px;width: 100%; ">
                                            <div class="search-slider"
                                                 style="width: 100%; display: flex; justify-content: space-between; position: relative;">
                                                <div class="left-search"></div>

                                                <div class="right-search">
                                                    <form action="searchcustomerbyname"method ="get">
                                                        Search Value: <input class="form-control" style="width: 250px;" type="text" name="search"/>          
                                                        <input type="submit" value="Search" name="btAction" />
                                                    </form><br/>
                                                    <p style="font-size: 24px; font-weight: bold ">STATUS</p>
                                                    <form action="searchcustomerbystatus" method="get">
                                                        <input type="submit" name="status" value="true" class="btn-link"/>
                                                        <input type="submit" name="status" value="false" class="btn-link"/>
                                                    </form>



                                                    <form action="sortaccount">
                                                        <select name="sort" class="form-control" name="Sort By">
                                                            <option value="Name">Name</option>
                                                            <option value="Email">Email</option>
                                                            <option value="Mobile">Phone</option>
                                                            <option value="Status">Status</option>
                                                        </select>
                                                        <input type="submit"/>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <form action="updatecustomer" method="get">
                                            <table class="panel-body table-responsive" id="slidetable" cellspacing="0" style="width: 100%;">
                                                <thead>
                                                    <tr style="cursor: pointer; font-size: 15px; border-bottom: 1px solid #ccc; text-align: center;">
                                                        <th>ID</th>
                                                        <th>NAME</th>
                                                        <th>EMAIL</th>
                                                        <th>PASSWORD</th>
                                                        <th>MOBILE</th>
                                                        <th>GENDER</th>
                                                        <th>ADDRESS</th>
                                                        <th>STATUS</th>
                                                    </tr>         
                                                </thead>
                                                <tbody>
                                                    <c:forEach items="${requestScope.cus}" var="cus">
                                                        <tr>
                                                            <td>${cus.id}</td>
                                                            <td> <a href="manageruserdetail?id=${cus.id}">
                                                                    ${cus.name} </a></td>
                                                            <td>${cus.email}</td>
                                                            <td>${cus.password}</td>
                                                            <td>${cus.mobile}</td>
                                                            <td>
                                                                <c:choose>
                                                                    <c:when test="${cus.gender eq true}">
                                                                        male
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        female
                                                                    </c:otherwise>
                                                                </c:choose>
                                                            </td>
                                                            <td>${cus.address}</td>

                                                            <c:if test="${cus.status==true}">
                                                                <td><button class="btn btn-warning" type="submit" name="updateStatus" value="${cus.id}">Hide</button></td>
                                                            </c:if>
                                                            <c:if test="${cus.status==false}">
                                                                <td><button class="btn btn-success" type="submit" name="updateStatus" value="${cus.id}">Show</button></td>
                                                            </c:if>
                                                        </tr>
                                                    </c:forEach>
                                                </tbody>
                                            </table>
                                        </form>
                                    </div><!-- /.box -->
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </aside>
        </div>

    </body>
</html>
