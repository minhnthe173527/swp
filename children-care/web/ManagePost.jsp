<%-- 
    Document   : managePost
    Created on : Oct 8, 2023, 12:49:52 AM
    Author     : asus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="./marketing/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="./marketing/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="./marketing/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link href="./marketing/css/morris/morris.css" rel="stylesheet" type="text/css" />
        <link href="./marketing/css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
        <link href="./marketing/css/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
        <link href="./marketing/css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
        <link href="./marketing/css/iCheck/all.css" rel="stylesheet" type="text/css" />
        <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
        <!-- Theme style -->
        <link href="./marketing/css/style.css" rel="stylesheet" type="text/css" />

        <style type="text/css">
            td{
                padding: 5px;
            }
            th{
                padding: 5px;
            }
        </style>
        <script src="ckeditor/ckeditor.js"></script> 
        <script src="ckfinder/ckfinder.js"></script>
        <title>Manage Post</title>
    </head>
    <body class="skin-black">
        <c:if test="${param['index']==null }">   
            <c:set var = "index" scope = "page" value = "1"/>
        </c:if>
        <c:if test="${param['index']!=null}">
            <c:set var = "index" scope = "page" value = "${param['index']}"/>
        </c:if>
        <!-- header logo: style can be found in header.less -->
        <div class="left-side sidebar-offcanvas"  style="height: 1200px;position: fixed; width: 220px; background-color: #39435c;left: 0; top:0;">
        </div>
        <header class="header">
            <a href="" class="logo">
                Manager
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <a href="logout" class="btn btn-outline-primary" style="text-decoration: none;  font-weight: 100;" >
                        <i class="fa fa-sign-out" style="font-size: 35px;" aria-hidden="true"></i>
                    </a>
                </div>
            </nav>
        </header>
        <!-- SIDEBAR -->
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas" id="left-aside" style="height: 100%;">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <c:if test="${sessionScope.u.getAvartar()==null}">
                                <img src="https://dvdn247.net/wp-content/uploads/2020/07/avatar-mac-dinh-1.png" class="img-circle" alt="User Image" />
                            </c:if>
                            <c:if test="${sessionScope.u.getAvartar()!=null}">
                                <img src="${sessionScope.u.getAvartar()}" class="img-circle" alt="User Image" />
                            </c:if>
                        </div>
                        <div class="pull-left info">
                            <p>Hello, ${sessionScope.u.getFullname()}</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu"> 
                        <li class="">
                            <a href="manageservicelist">
                                <i class="fa fa-circle"></i> <span>Service List</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="FeedbackList">
                                <i class="fa fa-circle"></i> <span>Feedback List</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="managerslider">
                                <i class="fa fa-circle"></i> <span>Sliders List</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="ManagePost">
                                <i class="fa fa-circle"></i> <span>Post List</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="manageruser">
                                <i class="fa fa-circle"></i> <span>Customer List</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="home">
                                <i class="fa fa-circle"></i> <span>Go Home</span>
                            </a>
                        </li>
                    </ul>
                </section>
                <!-- SIDEBAR --></aside>

            <aside class="right-side">

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="panel">
                                <header class="panel-heading">
                                    Manager User
                                </header>                
                                <div class="panel-body table-responsive">
                                    <div class="sliderList">
                                        <div class="input-group" style="margin-bottom: 10px;width: 100%; ">
                                            <div class="search-slider"
                                                 style="width: 100%; display: flex; justify-content: space-between; position: relative;">
                                                <div class="left-search"></div>

                                                <div class="right-search">
                                                    <a href="AddPost.jsp" class="btn-download">
                                                        <span class="text">Create Post</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>

                                        <table class="panel-body table-responsive" id="slidetable" cellspacing="0" style="width: 100%;">
                                            <thead>
                                                <tr style="cursor: pointer; font-size: 15px; border-bottom: 1px solid #ccc; text-align: center;">
                                                    <th>Post ID</th>
                                                    <th style=" padding-left: 77px;">Image</th>
                                                    <th style=" padding-left: 254px;">Content</th>
                                                    <th  style=" padding-left: 52px;">Status</th>
                                                </tr>         
                                            </thead>
                                            <tbody>
                                                <c:forEach items="${post}" var="p">
                                                    <tr>
                                                        <td><a style="color: black" href="ManagePostDetail?id=${p.getPostId()}">${p.getPostId()}</a></td>
                                                        <td>
                                                            <a style="color: black" href="ManagePostDetail?id=${p.getPostId()}">   <img style="width:200px" src="${p.getImage()}"></a>
                                                        </td>
                                                        <td> <a style="color: black" href="ManagePostDetail?id=${p.getPostId()}">${p.getContent()} </a></td>
                                                        <td><span class="btn btn-success">${p.getStatus()}</span></td>
                                                    </tr>	
                                                </c:forEach>
                                            </tbody>
                                        </table>

                                    </div><!-- /.box -->
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </aside>

<!--             CONTENT 
            <section id="content">
                 NAVBAR 
                <nav>
                    <i class='bx bx-menu' ></i>
                    <a href="#" class="nav-link"></a>
                </nav>
                 NAVBAR 

                 MAIN 
                <main>
                    <div class="head-title">

                        <a href="AddPost.jsp" class="btn-download">
                            <span class="text">Create Post</span>
                        </a>
                    </div>



                    <table>
                        <thead>
                            <tr>
                                <th>Post ID</th>
                                <th style=" padding-left: 77px;">Image</th>
                                <th style=" padding-left: 254px;">Content</th>
                                <th style=" padding-left: 52px;">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${post}" var="p">
                                <tr>
                                    <td><a style="color: black" href="ManagePostDetail?id=${p.getPostId()}">${p.getPostId()}</a></td>
                                    <td>
                                        <a style="color: black" href="ManagePostDetail?id=${p.getPostId()}">   <img src="${p.getImage()}"></a>
                                    </td>
                                    <td> <a style="color: black" href="ManagePostDetail?id=${p.getPostId()}">${p.getContent()} </a></td>
                                    <td><span class="status completed">${p.getStatus()}</span></td>
                                </tr>	
                            </c:forEach>
                        </tbody>
                    </table>


                </main>
                 MAIN 
            </section>-->
            <!-- CONTENT -->
        </div>
            <script src="managepost/script.js"></script>
    </body>
</html>
