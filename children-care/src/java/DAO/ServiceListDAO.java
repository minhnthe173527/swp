/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;

import Model.ServiceList;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author minh1
 *
 * private int serviceId; private String serviceName; private String
 * description; private double price; private int categoryId; private String
 * image;
 */
public class ServiceListDAO extends DBContext {

    private List<ServiceList> servicelist;

    public List<ServiceList> getService() {
        return servicelist;
    }

    public List<ServiceList> getAllService() {
        List<ServiceList> list = new ArrayList<>();
        String sql = "select * from Service";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                ServiceList sv = ServiceList.builder()
                        .serviceId(rs.getInt(1))
                        .name(rs.getString(2))
                        .description(rs.getString(3))
                        .price(rs.getDouble(4))
                        .categoryId(rs.getInt(5))
                        .image(rs.getString(6))
                        .status(rs.getBoolean(7))
                        .build();
                list.add(sv);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<ServiceList> SearchService(String searchValue) {
        List<ServiceList> list = new ArrayList<>();
        String sql = "select * from Service where ServiceName LIKE ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + searchValue + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                ServiceList sv = ServiceList.builder()
                        .serviceId(rs.getInt(1))
                        .name(rs.getString(2))
                        .description(rs.getString(3))
                        .price(rs.getDouble(4))
                        .categoryId(rs.getInt(5))
                        .image(rs.getString(6))
                        .status(rs.getBoolean(7))
                        .build();
                list.add(sv);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<ServiceList> ListServiceByCate(String Value) {
        List<ServiceList> list = new ArrayList<>();
        String sql = "SELECT s.ServiceID, s.ServiceName, s.Description, s.Price, s.Image, s.Status\n"
                + "FROM Service s\n"
                + "INNER JOIN ServiceCategory sc ON s.CategoryID = sc.CID\n"
                + "WHERE sc.CName like ?;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + Value + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                ServiceList sv = ServiceList.builder()
                        .serviceId(rs.getInt(1))
                        .name(rs.getString(2))
                        .description(rs.getString(3))
                        .price(rs.getDouble(4))
                        .image(rs.getString(5))
                        .status(rs.getBoolean(6))
                        .build();
                list.add(sv);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public int getQuantityService() {
        String sql = "select count(*) from Service";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return 0;
    }
    
    public List<ServiceList> paggingService(int index) {
        List<ServiceList> list = new ArrayList<>();
        String sql = "Select * from Service\n"
                + "order by ServiceID\n"
                + "OFFSET ? row fetch next 4 rows only;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, (index - 1) * 4);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                ServiceList sv = ServiceList.builder()
                        .serviceId(rs.getInt(1))
                        .name(rs.getString(2))
                        .description(rs.getString(3))
                        .price(rs.getDouble(4))
                        .categoryId(rs.getInt(5))
                        .image(rs.getString(6))
                        .status(rs.getBoolean(7))
                        .build();
                list.add(sv);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public static void main(String[] args) {
        ServiceListDAO dao = new ServiceListDAO();
        List<ServiceList> list = dao.ListServiceByCate("Medical Check-up");
        for (ServiceList o : list) {
            System.out.println(o);
        }
    }

}
