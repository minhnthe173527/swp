/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controllers.Common;

import DAO.AccountDAO;
import Model.Account;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/**
 *
 * @author asus
 */
public class ChangeProfile extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String name = request.getParameter("name");
        String email = request.getParameter("email");
        String phone = request.getParameter("phone");
        String gender = request.getParameter("gender");
        Boolean gen = Boolean.parseBoolean(gender);
        String id = request.getParameter("id");
        HttpSession session = request.getSession();
        Account u = (Account) session.getAttribute("account");
        AccountDAO dao = new AccountDAO();
        //change data in db
        if(u.getRole().equals("customer")){
            dao.updateProfile(name, email, phone, gen, "UserAccount");
        }else{
            dao.updateProfile(name, email, phone, gen, "EmployeeAccount");
        }
        //change data on web
        Account acc = new Account();
        acc.setFullname(name);
        acc.setEmail(email);
        acc.setMobile(phone);
        acc.setGender(gen);
        acc.setRole(u.getRole());
        acc.setId(u.getId()); 
        acc.setStatus(u.getStatus());            
        session.removeAttribute("account");
        session.setAttribute("account", acc);
        request.setAttribute("mess", "Updated successfully");
        request.getRequestDispatcher("Profile.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
