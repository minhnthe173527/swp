/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;

import Model.Post;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author minh1
 */
public class PostDAO extends DBContext {

    public Post getPostById(String id) {
        String sql = "SELECT [PostID]\n"
                + "      ,[Image]\n"
                + "      ,[Content]\n"
                + "      ,[Featuring]\n"
                + "      ,[Status]\n"
                + "  FROM [dbo].[Post] where PostID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Post c = new Post();
                c.setPostId(rs.getInt("postID"));
                c.setImage(rs.getString("image"));
                c.setContent(rs.getString("content"));
                c.setFeaturing(rs.getString("featuring"));
                c.setStatus(rs.getString("status"));
                return c;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public List<Post> getPost() {
        List<Post> post = new ArrayList<>();
        String sql = "SELECT * FROM [Post] order by PostID desc";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                post.add(new Post(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5)));
            }

        } catch (SQLException e) {

        }
        return post;
    }

    public void addPost(int id, String image, String content, String featuring) {
        try {
            String sql = "  Insert into Post([PostID], [Image], [Content], [Featuring], [Status]) values (?,?,?,?, 'accept')";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, id);
            ps.setString(2, image);
            ps.setString(3, content);
            ps.setString(4, featuring);
            ps.executeUpdate();
        } catch (SQLException e) {

        }
    }

    public List<Post> getAllPost() {
        List<Post> ls = new ArrayList<>();
        String sql = "SELECT [PostID]\n"
                + "      ,[Image]\n"
                + "      ,[Content]\n"
                + "      ,[Featuring]\n"
                + "      ,[Status]\n"
                + "  FROM [dbo].[Post]";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Post c = new Post();
                c.setPostId(rs.getInt("postID"));
                c.setImage(rs.getString("image"));
                c.setContent(rs.getString("content"));
                c.setFeaturing(rs.getString("featuring"));
                c.setStatus(rs.getString("status"));
                ls.add(c);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return ls;
    }


    public static void main(String[] args) {
        PostDAO d = new PostDAO();
        List<Post> lp = d.getAllPost();
        for (Post post : lp) {
            System.out.println(post);

        }
    }
}
