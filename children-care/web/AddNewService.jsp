<%-- 
    Document   : AddNewService
    Created on : Oct 14, 2023, 10:16:28 PM
    Author     : ADMIN
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Boxicons -->
        <link href="./marketing/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="./marketing/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="./marketing/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link href="./marketing/css/morris/morris.css" rel="stylesheet" type="text/css" />
        <link href="./marketing/css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
        <link href="./marketing/css/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
        <link href="./marketing/css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
        <link href="./marketing/css/iCheck/all.css" rel="stylesheet" type="text/css" />
        <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
        <!-- Theme style -->
        <link href="./marketing/css/style.css" rel="stylesheet" type="text/css" />

        <style type="text/css">
            td{
                padding: 5px;
            }
            th{
                padding: 5px;
            }
        </style>
        <script src="ckeditor/ckeditor.js"></script> 
        <script src="ckfinder/ckfinder.js"></script>
        <title>Add New Service</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                background-color: #f2f2f2;
            }
            h1 {
                color: #333;
            }
            form {
                background-color: #fff;
                padding: 20px;
                border: 1px solid #ccc;
                border-radius: 5px;
                width: 300px;
                margin: 0 auto;
            }
            table {
                width: 100%;
            }
            td {
                padding: 10px;
            }
            input[type="text"],
            input[type="number"],
            select {
                width: 100%;
                padding: 8px;
                margin: 5px 0;
                border: 1px solid #ccc;
                border-radius: 3px;
            }
            input[type="submit"],
            a.btn {
                display: inline-block;
                padding: 10px 20px;
                margin: 10px 0;
                background-color: #007bff;
                color: #fff;
                text-decoration: none;
                border: none;
                border-radius: 5px;
            }
            a.btn.btn-secondary {
                background-color: #ccc;
            }
            img.card-img-top {
                max-width: 100%;
            }
        </style>
    </head>
    <body class="skin-black">
        <c:if test="${param['index']==null }">   
            <c:set var = "index" scope = "page" value = "1"/>
        </c:if>
        <c:if test="${param['index']!=null}">
            <c:set var = "index" scope = "page" value = "${param['index']}"/>
        </c:if>
        <!-- header logo: style can be found in header.less -->
        <div class="left-side sidebar-offcanvas"  style="height: 1200px;position: fixed; width: 220px; background-color: #39435c;left: 0; top:0;">
        </div>
        <header class="header">
            <a href="" class="logo">
                Manager
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <a href="logout" class="btn btn-outline-primary" style="text-decoration: none;  font-weight: 100;" >
                        <i class="fa fa-sign-out" style="font-size: 35px;" aria-hidden="true"></i>
                    </a>
                </div>
            </nav>
        </header>
        <!-- SIDEBAR -->
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas" id="left-aside" style="height: 100%;">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <c:if test="${sessionScope.u.getAvartar()==null}">
                                <img src="https://dvdn247.net/wp-content/uploads/2020/07/avatar-mac-dinh-1.png" class="img-circle" alt="User Image" />
                            </c:if>
                            <c:if test="${sessionScope.u.getAvartar()!=null}">
                                <img src="${sessionScope.u.getAvartar()}" class="img-circle" alt="User Image" />
                            </c:if>
                        </div>
                        <div class="pull-left info">
                            <p>Hello, ${sessionScope.u.getFullname()}</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu"> 
                        <li class="">
                            <a href="manageservicelist">
                                <i class="fa fa-circle"></i> <span>Service List</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="FeedbackList">
                                <i class="fa fa-circle"></i> <span>Feedback List</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="managerslider">
                                <i class="fa fa-circle"></i> <span>Sliders List</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="ManagePost">
                                <i class="fa fa-circle"></i> <span>Post List</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="manageruser">
                                <i class="fa fa-circle"></i> <span>Customer List</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="home">
                                <i class="fa fa-circle"></i> <span>Go Home</span>
                            </a>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>
            <h2 style="text-align: center">Add Service</h2>
            <form action="manageservicelist" method="POST">
                <table>
                    <tr>
                        <td>Image: </td>
                        <td><img class="card-img-top" src="images/" alt=""></td>
                    </tr>
                    <tr>
                        <td>Service ID: </td>
                        <td><input type="text" name="svId" value=""></td>
                    </tr>
                    <tr>
                        <td>Service Name: </td>
                        <td><input type="text" name="svName" value=""></td>
                    </tr>
                    <tr>
                        <td>Description: </td>
                        <td><input type="text" name="svDescription" value=""></td>
                    </tr>
                    <tr>
                        <td>Price: </td>
                        <td><input type="number" name="svPrice" value=""></td>
                    </tr>
                    <tr>
                        <td>Category: </td>
                        <td><select name="svCategory">
                                <option value="1">Medical Check-up</option>
                                <option value="2">Medical Consultation</option>
                                <option value="3">Health Care</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Status: </td>
                        <td><select name="svStatus">
                                <option value="true">Active</option>
                                <option value="false">Inactive</option>
                            </select>
                        </td>
                    </tr>
                </table>
                <input type="submit" name="btnAdd" value="Add" class="btn btn-primary">
                <a href="manageservicelist" class="btn btn-secondary">Back</a>
            </form>
            <h2>${mess}</h2>
        </div>
    </body>
</html>
