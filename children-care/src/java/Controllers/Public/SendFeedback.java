/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controllers.Public;

import DAO.ServiceCategoryDAO;
import DAO.ServiceDAO;
import Model.Account;
import Model.Service;
import Model.ServiceCategory;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import DAO.FeedBackDAO;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.Part;
import java.io.File;

/**
 *
 * @author SANG
 */
@MultipartConfig
public class SendFeedback extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SendFeedback</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SendFeedback at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        HttpSession session = request.getSession();
        String serviceidp = request.getParameter("serviceid");
        ServiceDAO sd = new ServiceDAO();
        Service sv = sd.getServiceById(serviceidp);
        ServiceCategoryDAO ctd = new ServiceCategoryDAO();
        List<ServiceCategory> listcate = ctd.getAllServiceCategory();
        request.setAttribute("listcategory", listcate);
        request.setAttribute("service", sv);
        request.getRequestDispatcher("Feedback.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        FeedBackDAO dao = new FeedBackDAO();
//        HttpSession session = request.getSession();
//        Account u = (Account) session.getAttribute("u");
        Account u = new Account();
        u.setId(1);
        String feedback = request.getParameter("feedback");
        String rate = request.getParameter("rate");
        String pid = request.getParameter("sid");
//        String img = request.getParameter("fbimg");

        Part part = request.getPart("fbimg");
        System.out.println(part);

        String fileName = part.getSubmittedFileName();
        System.out.println(fileName);
        String path2 = getServletContext().getRealPath("") + "images";

        String path_new = path2.replace(String.valueOf("\\build"), "");

        File file = new File(path_new);
        part.write(path_new + File.separator + fileName);
        String msg;
        boolean c = dao.addAnewFeedBack(u.getId(), feedback, pid, fileName, rate);
        if (c) {
            msg = "Send feedback success";
        } else {
            msg = "Send feedback failed";
        }
        request.setAttribute("msg", msg);
        request.getRequestDispatcher("Feedback.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
