package Model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;



/**
 *
 * @author ADMIN
 */
@Builder
@Getter
@Setter
@ToString
public class ServiceList {
    private int serviceId;
    private String name;
    private String description;
    private double price;
    private int categoryId;
    private String image;
    private boolean status;
}