/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;



/**
 *
 * @author ADMIN
 */

public class ManagerAcc {
    private int managerId;
    private String fullname;
    private String email;
    private String password;
    private String mobile;
    private boolean gender;
    private String role;
    private String status;
    private String address; 

    public ManagerAcc(int managerId, String fullname, String email, String password, String mobile, boolean gender, String role, String status, String address) {
        this.managerId = managerId;
        this.fullname = fullname;
        this.email = email;
        this.password = password;
        this.mobile = mobile;
        this.gender = gender;
        this.role = role;
        this.status = status;
        this.address = address;
    }
    public ManagerAcc() {
    }

    public int getManagerId() {
        return managerId;
    }

    public void setManagerId(int managerId) {
        this.managerId = managerId;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public boolean isGender() {
        return gender;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    
    
    
}
