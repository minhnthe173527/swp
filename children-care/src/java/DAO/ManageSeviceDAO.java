/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;

import Model.ServiceList;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ADMIN
 */
public class ManageSeviceDAO extends DBContext {

    public List<ServiceList> getAllService() {
        List<ServiceList> list = new ArrayList<>();
        String sql = "select * from Service";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                ServiceList sv = ServiceList.builder()
                        .serviceId(rs.getInt(1))
                        .name(rs.getString(2))
                        .description(rs.getString(3))
                        .price(rs.getDouble(4))
                        .categoryId(rs.getInt(5))
                        .image(rs.getString(6))
                        .status(rs.getBoolean(7))
                        .build();
                list.add(sv);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public void changeStatusbyId(int id, int status) {
        String sql = "Update Service set Status = ? where ServiceID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, status);
            st.setInt(2, id);
            st.executeUpdate();
        } catch (SQLException e) {
            System.err.println(e);
        }
    }

    public int getQuantityService() {
        String sql = "select count(*) from Service";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return 0;
    }

    public List<ServiceList> paggingService(int index) {
        List<ServiceList> list = new ArrayList<>();
        String sql = "Select * from Service\n"
                + "order by ServiceID\n"
                + "OFFSET ? row fetch next 4 rows only;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, (index - 1) * 4);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                ServiceList sv = ServiceList.builder()
                        .serviceId(rs.getInt(1))
                        .name(rs.getString(2))
                        .description(rs.getString(3))
                        .price(rs.getDouble(4))
                        .categoryId(rs.getInt(5))
                        .image(rs.getString(6))
                        .status(rs.getBoolean(7))
                        .build();
                list.add(sv);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public ServiceList getServicebyId(int index) {
        String sql = "select * from Service where ServiceID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, index);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                ServiceList sv = ServiceList.builder()
                        .serviceId(rs.getInt(1))
                        .name(rs.getString(2))
                        .description(rs.getString(3))
                        .price(rs.getDouble(4))
                        .categoryId(rs.getInt(5))
                        .image(rs.getString(6))
                        .status(rs.getBoolean(7))
                        .build();
                return sv;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public List<ServiceList> getServicebyStatus(int index) {
        List<ServiceList> list = new ArrayList<>();
        String sql = "select * from Service where Status = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, index);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                ServiceList sv = ServiceList.builder()
                        .serviceId(rs.getInt(1))
                        .name(rs.getString(2))
                        .description(rs.getString(3))
                        .price(rs.getDouble(4))
                        .categoryId(rs.getInt(5))
                        .image(rs.getString(6))
                        .status(rs.getBoolean(7))
                        .build();
                list.add(sv);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<ServiceList> getServicesortbyName() {
        List<ServiceList> list = new ArrayList<>();
        String sql = "select * from Service order by ServiceName asc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                ServiceList sv = ServiceList.builder()
                        .serviceId(rs.getInt(1))
                        .name(rs.getString(2))
                        .description(rs.getString(3))
                        .price(rs.getDouble(4))
                        .categoryId(rs.getInt(5))
                        .image(rs.getString(6))
                        .status(rs.getBoolean(7))
                        .build();
                list.add(sv);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<ServiceList> getServicesortbyPrice() {
        List<ServiceList> list = new ArrayList<>();
        String sql = "select * from Service order by Price asc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                ServiceList sv = ServiceList.builder()
                        .serviceId(rs.getInt(1))
                        .name(rs.getString(2))
                        .description(rs.getString(3))
                        .price(rs.getDouble(4))
                        .categoryId(rs.getInt(5))
                        .image(rs.getString(6))
                        .status(rs.getBoolean(7))
                        .build();
                list.add(sv);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<ServiceList> getServicesortbyCategoryId() {
        List<ServiceList> list = new ArrayList<>();
        String sql = "select * from Service order by CategoryID asc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                ServiceList sv = ServiceList.builder()
                        .serviceId(rs.getInt(1))
                        .name(rs.getString(2))
                        .description(rs.getString(3))
                        .price(rs.getDouble(4))
                        .categoryId(rs.getInt(5))
                        .image(rs.getString(6))
                        .status(rs.getBoolean(7))
                        .build();
                list.add(sv);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<ServiceList> getServicesortbyStatus() {
        List<ServiceList> list = new ArrayList<>();
        String sql = "select * from Service order by Status asc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                ServiceList sv = ServiceList.builder()
                        .serviceId(rs.getInt(1))
                        .name(rs.getString(2))
                        .description(rs.getString(3))
                        .price(rs.getDouble(4))
                        .categoryId(rs.getInt(5))
                        .image(rs.getString(6))
                        .status(rs.getBoolean(7))
                        .build();
                list.add(sv);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public void updateServicebyId(int id, String name, String description, double price, int categoryId) {
        String sql = "update Service "
                + "set ServiceName=?"
                + ",Description=?"
                + ",Price=?"
                + ",CategoryID=?"
                + " where ServiceID = ? ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, name);
            st.setString(2, description);
            st.setDouble(3, price);
            st.setInt(4, categoryId);
            st.setInt(5, id);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public boolean isDuplicateService(int id, String name) {
        String sql = "SELECT COUNT(*) FROM Service WHERE ServiceID = ? OR ServiceName = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            st.setString(2, name);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                int count = rs.getInt(1);
                return count > 0;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return false;
    }

    public void createService(int id, String name, String description, double price, int categoryId, String image, int status) {

        String insertSql = "INSERT INTO Service (ServiceID, ServiceName, Description, Price, CategoryID, Image, Status) VALUES (?,?,?,?,?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(insertSql);
            st.setInt(1, id);
            st.setString(2, name);
            st.setString(3, description);
            st.setDouble(4, price);
            st.setInt(5, categoryId);
            st.setString(6, image);
            st.setInt(7, status);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public int getQuantityServiceCategory() {
        String sql = "select count(*) from ServiceCategory";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return 0;
    }

    public static void main(String[] args) {
        ManageSeviceDAO dao = new ManageSeviceDAO();
        List<ServiceList> list = dao.getServicesortbyStatus();
        for (ServiceList o : list) {
            System.out.println(o);
        }
    }
}
