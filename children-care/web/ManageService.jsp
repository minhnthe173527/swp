<%-- 
    Document   : ManageService
    Created on : Oct 9, 2023, 9:31:26 AM
    Author     : ADMIN
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="./marketing/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="./marketing/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="./marketing/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link href="./marketing/css/morris/morris.css" rel="stylesheet" type="text/css" />
        <link href="./marketing/css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
        <link href="./marketing/css/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
        <link href="./marketing/css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
        <link href="./marketing/css/iCheck/all.css" rel="stylesheet" type="text/css" />
        <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
        <!-- Theme style -->
        <link href="./marketing/css/style.css" rel="stylesheet" type="text/css" />
        <style type="text/css">
            td{
                padding: 5px;
            }
            th{
                padding: 5px;
            }
            .card-img-top {
                width: 20%;
                height: auto;
            }
        </style>
    </head>
    <body class="skin-black">
        <c:if test="${param['index']==null }">   
            <c:set var = "index" scope = "page" value = "1"/>
        </c:if>
        <c:if test="${param['index']!=null}">
            <c:set var = "index" scope = "page" value = "${param['index']}"/>
        </c:if>
        <!-- header logo: style can be found in header.less -->
        <div class="left-side sidebar-offcanvas"  style="height: 1200px;position: fixed; width: 220px; background-color: #39435c;left: 0; top:0;">
        </div>
        <header class="header">
            <a href="" class="logo">
                Manager
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <a href="logout" class="btn btn-outline-primary" style="text-decoration: none;  font-weight: 100;" >
                        <i class="fa fa-sign-out" style="font-size: 35px;" aria-hidden="true"></i>
                    </a>
                </div>
            </nav>
        </header>


        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas" id="left-aside" style="height: 100%;">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <c:if test="${sessionScope.u.getAvartar()==null}">
                                <img src="https://dvdn247.net/wp-content/uploads/2020/07/avatar-mac-dinh-1.png" class="img-circle" alt="User Image" />
                            </c:if>
                            <c:if test="${sessionScope.u.getAvartar()!=null}">
                                <img src="${sessionScope.u.getAvartar()}" class="img-circle" alt="User Image" />
                            </c:if>
                        </div>
                        <div class="pull-left info">
                            <p>Hello, ${sessionScope.u.getFullname()}</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu"> 
                        <li class="">
                            <a href="manageservicelist">
                                <i class="fa fa-circle"></i> <span>Service List</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="FeedbackList">
                                <i class="fa fa-circle"></i> <span>Feedback List</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="managerslider">
                                <i class="fa fa-circle"></i> <span>Sliders List</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="ManagePost">
                                <i class="fa fa-circle"></i> <span>Post List</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="manageruser">
                                <i class="fa fa-circle"></i> <span>Customer List</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="home">
                                <i class="fa fa-circle"></i> <span>Go Home</span>
                            </a>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>
            <aside class="right-side">

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="panel">
                                <header class="panel-heading">
                                    Manager Service
                                </header>                
                                <div class="panel-body table-responsive">
                                    <div class="sliderList">
                                        <div class="input-group" style="margin-bottom: 10px;width: 100%; ">
                                            <div class="search-slider"
                                                 style="width: 100%; display: flex; justify-content: space-between; position: relative;">
                                                <div class="left-search"></div>

                                                <div class="right-search">
                                                    <form action="manageservicelist">
                                                        <input type="text" class="form-control" name="txtSearchValue" value="${param.txtSearchValue}">
                                                    </form><br/>
                                                    <a href="AddNewService.jsp" class="btn btn-primary">Add New Service</a>
                                                    <p style="font-size: 24px; font-weight: bold ">STATUS</p>
                                                    <c:if test="${filter == false}"><a href="manageservicelist?filterStatus=false">Hide</a></c:if>
                                                    <c:if test="${filter == true}"><a href="manageservicelist?filterStatus=true">Show</a></c:if>



                                                        <form action="manageservicelist">
                                                            <label for="sortSelect">Sort By:</label>
                                                            <select id="sortSelect" name="sortField">
                                                                <option value="ServiceName">Service Name</option>
                                                                <option value="Category">Category</option>
                                                                <option value="Price">Price</option>
                                                                <option value="Status">Status</option>
                                                            </select>
                                                            <input type="submit" value="Sort">
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <form action="manageservicelist" method="get">
                                                <table class="panel-body table-responsive" id="slidetable" cellspacing="0" style="width: 100%;">
                                                    <thead>
                                                        <tr style="cursor: pointer; font-size: 15px; border-bottom: 1px solid #ccc; text-align: center;">
                                                            <th>ID</th>
                                                            <th>Service Name</th>
                                                            <th>Description</th>
                                                            <th>Price</th>
                                                            <th>Category</th>
                                                            <th>Image</th>
                                                            <th>Status</th>
                                                            <th>Action</th>
                                                        </tr>         
                                                    </thead>
                                                    <tbody>
                                                    <c:forEach var="sv" items="${listsvp}">
                                                        <tr>
                                                            <td>${sv.serviceId}</td>
                                                            <td>${sv.name}</td>
                                                            <td>${sv.description}</td>
                                                            <td>${sv.price}</td>
                                                            <td>${sv.categoryId}</td>
                                                            <td><img class="card-img-top" src="images/${sv.image}" alt=""></td>
                                                            <td><a href="changeservicestatus?id=${sv.serviceId}&status=${!sv.status}">
                                                                    <c:choose>
                                                                        <c:when test="${!sv.status}">
                                                                            Deactive
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            Active
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </a>
                                                            </td>
                                                            <td><a href="manageservicedetail?svId=${sv.serviceId}">Detail</a>                        
                                                            </td>
                                                        </tr>
                                                    </c:forEach>
                                                </tbody>
                                            </table>
                                        </form>
                                        <div class="pagination-arena ">
                                            <c:forEach begin="1" end="${endP}" var="i">
                                                <a class="${tag==i?"active":""}" href="manageservicelist?page=${i}">${i}</a>
                                            </c:forEach>
                                        </div><!-- /.box -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </aside>
        </div>
    </body>
</html>
