/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;

import Model.Setting;
import com.oracle.wls.shaded.org.apache.xalan.xsltc.cmdline.getopt.GetOpt;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author ktbinh2003
 */
public class SettingDAO extends DBContext {

    public static void main(String[] args) {
        SettingDAO deo = new SettingDAO();
        deo.addNew(new Setting(deo.getLastId()+1, "asd", "Asdf", "asdf", "sdfggfh"));
        System.out.println(deo.getLastId());
        System.out.println(deo.findById(deo.getLastId()));
    }

    public void addNew(Setting setting) {
        String sql = "INSERT INTO [dbo].[Setting]\n"
                + "           ([SettingID]\n"
                + "           ,[Name]\n"
                + "           ,[Value]\n"
                + "           ,[Description]\n"
                + "           ,[Status])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?)";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, setting.getSettingId());
            ps.setString(2, setting.getName());
            ps.setString(3, setting.getValue());
            ps.setString(4, setting.getDescription());
            ps.setString(5, setting.getStatus());
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public int getLastId() {
        String sql = "SELECT TOP 1 [SettingID]\n"
                + "  FROM [dbo].[Setting]\n"
                + "  ORDER BY [SettingID] desc";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public void save(Setting setting) {
        String sql = "UPDATE [dbo].[Setting]\n"
                + "   SET [Name] = ?\n"
                + "      ,[Value] = ?\n"
                + "      ,[Description] = ?\n"
                + "      ,[Status] = ?\n"
                + " WHERE [SettingID] =?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, setting.getName());
            ps.setString(2, setting.getValue());
            ps.setString(3, setting.getDescription());
            ps.setString(4, setting.getStatus());
            ps.setInt(5, setting.getSettingId());
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public Setting findById(int id) {
        Setting s = new Setting();
        String sql = "SELECT [SettingID]\n"
                + "      ,[Name]\n"
                + "      ,[Value]\n"
                + "      ,[Description]\n"
                + "      ,[Status]\n"
                + "  FROM [SWP].[dbo].[Setting]\n"
                + "  WHERE [SettingID] = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                s.setSettingId(rs.getInt(1));
                s.setName(rs.getString(2));
                s.setValue(rs.getString(3));
                s.setDescription(rs.getString(4));
                s.setStatus(rs.getString(5));
            }
        } catch (Exception e) {
        }
        return s;
    }

    public ArrayList<Setting> sort(String sortField, String sortDir, int index) {
        ArrayList<Setting> list = new ArrayList<>();
        String sql = "SELECT [SettingID]\n"
                + "      ,[Name]\n"
                + "      ,[Value]\n"
                + "      ,[Description]\n"
                + "      ,[Status]\n"
                + "  FROM [SWP].[dbo].[Setting]\n"
                + "  ORDER BY [" + sortField + "] " + sortDir + " "
                + "  OFFSET ? ROWS FETCH NEXT 5 ROWS ONLY;";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, (index - 1) * 5);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Setting(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public int getTotalSearch(String search) {
        String sql = "SELECT COUNT(*) FROM [SWP].[dbo].[Setting] "
                + "WHERE [Name] LIKE ? OR [Value] LIKE ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, "%" + search + "%");
            ps.setString(2, "%" + search + "%");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public ArrayList<Setting> search(String search, int index) {
        ArrayList<Setting> list = new ArrayList<>();
        String sql = "SELECT [SettingID]\n"
                + "      ,[Name]\n"
                + "      ,[Value]\n"
                + "      ,[Description]\n"
                + "      ,[Status]\n"
                + "  FROM [SWP].[dbo].[Setting]\n"
                + "  WHERE [Name] LIKE ? OR [Value] LIKE ? "
                + "  ORDER BY [SettingID] OFFSET ? \n"
                + "  ROWS FETCH NEXT 5 ROWS ONLY;";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, "%" + search + "%");
            ps.setString(2, "%" + search + "%");
            ps.setInt(3, (index - 1) * 5);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Setting(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    //get Total number rows in setting table
    public int getTotalSettings() {
        String sql = "SELECT COUNT(*) FROM [SWP].[dbo].[Setting]";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }

    //get All Status
    public ArrayList<String> getAllStatus() {
        ArrayList<String> list = new ArrayList<>();
        String sql = "SELECT DISTINCT [Status] FROM [SWP].[dbo].[Setting];";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(rs.getString(1));
            }
        } catch (Exception e) {

        }
        return list;
    }

    public int getTotalSettingsStatus(String status) {
        String sql = "SELECT COUNT(*) \n"
                + "FROM [SWP].[dbo].[Setting]\n"
                + "WHERE [Status] LIKE ?\n";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, "%" + status + "%");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public ArrayList<Setting> filterStatus(String status, int index) {
        ArrayList<Setting> list = new ArrayList<>();
        String sql = "SELECT [SettingID],[Name],[Value] ,[Description] ,[Status] \n"
                + "FROM [SWP].[dbo].[Setting]\n"
                + "WHERE [Status] LIKE ?\n"
                + "ORDER BY [SettingID] OFFSET ? \n"
                + "ROWS FETCH NEXT 5 ROWS ONLY;";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, "%" + status + "%");
            ps.setInt(2, (index - 1) * 5);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Setting(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5)));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return list;
    }

    public ArrayList<Setting> pagingSetting(int index) {
        ArrayList<Setting> list = new ArrayList<>();
        String sql = "SELECT [SettingID]\n"
                + "      ,[Name]\n"
                + "      ,[Value]\n"
                + "      ,[Description]\n"
                + "      ,[Status]\n"
                + "  FROM [SWP].[dbo].[Setting]\n"
                + "  ORDER BY [SettingID]\n"
                + "  OFFSET ? ROWS FETCH NEXT 5 ROWS ONLY;";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, (index - 1) * 5);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Setting(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    //Get all rows in setting table
    public ArrayList<Setting> getAllSetting() {
        ArrayList<Setting> list = new ArrayList<>();
        String sql = "SELECT [SettingID]\n"
                + "      ,[Name]\n"
                + "      ,[Value]\n"
                + "      ,[Description]\n"
                + "      ,[Status]\n"
                + "  FROM [SWP].[dbo].[Setting]";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Setting(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5)));
            }
        } catch (Exception e) {
        }
        return list;
    }
}
