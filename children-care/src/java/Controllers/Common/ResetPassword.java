/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controllers.Common;

import DAO.AccountDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Date;

/**
 *
 * @author Lenovo
 */
public class ResetPassword extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String encodedData = request.getParameter("data");
        long currentTime = new Date().getTime() / 1000;
        if (encodedData != null) {
            //decode data
            byte[] data = Base64.getDecoder().decode(encodedData);
            String decodedData = new String(data, StandardCharsets.UTF_8);//convert decoded data to string
            //get information from decoded data
            String[] info = decodedData.split("\\|");
            //check url expired
            if (Integer.parseInt(info[1]) < currentTime) {
                out.print("Expired! Click <a href='resetreq'>here</a>");
                return;
            }
            //go to webpage to let user enter password
            request.setAttribute("email", info[0]);
            request.setAttribute("expire", info[1]);
            request.getRequestDispatcher("ResetPassword.jsp").forward(request, response);
            return;
        }
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String cnfPassword = request.getParameter("cnfpassword");
        String expire = request.getParameter("expire");
        //check error
        if (email == null || expire == "") {
            out.println("Request error, please get reset link from <a href='resetreq'>here</a>");
            return;
        }
        //check expire again
        if (Integer.parseInt(expire) < currentTime) {
            out.print("Expired! Click <a href='resetreq'>here</a>");
            return;
        }
        //check password format
        String regex = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$";
        if (!password.matches(regex)) {
            request.setAttribute("email", email);
            request.setAttribute("expire", expire);
            request.setAttribute("msg", "Passwords must have at least 8 characters and contain at least one uppercase letters, lowercase letters, numbers, or symbols.");
            request.getRequestDispatcher("ResetPassword.jsp").forward(request, response);
            return;
        }
        //check password matches
        if (!password.equals(cnfPassword)) {
            request.setAttribute("email", email);
            request.setAttribute("expire", expire);
            request.setAttribute("msg", "Confirm password must be matches to new password");
            request.getRequestDispatcher("ResetPassword.jsp").forward(request, response);
            return;
        }
        //change password
        AccountDAO dao = new AccountDAO();
        dao.changePassword(email, password, "UserAccount");
        dao.changePassword(email, password, "EmployeeAccount");
       out.println("<script>");
        out.println("var confirmation = confirm('Password changed successfully, back to login?')");
        out.println("if (confirmation) {");
        out.println("window.location.href = 'login';");
        out.println("}");
        out.println("</script>");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
