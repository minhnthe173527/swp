/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;


import Model.ServiceCategory;
import Model.ServiceCategory;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author minh1
 */
public class ServiceCategoryDAO extends DBContext {

    public List<ServiceCategory> getAllServiceCategory() {
        List<ServiceCategory> ls = new ArrayList<>();
        String sql = "SELECT [Cid]\n"
                + "      ,[Cname]\n"
                + "  FROM [dbo].[ServiceCategory]";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                ServiceCategory s = new ServiceCategory();
                s.setCid(rs.getInt("cid"));
                s.setCname(rs.getString("cname"));
                ls.add(s);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return ls;
    }

}
