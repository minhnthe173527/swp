<%-- 
    Document   : detailpost
    Created on : Sep 20, 2023, 1:38:00 PM
    Author     : minh1
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link rel="stylesheet" href="css/style.css">
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="assets/css/fontawesome.css">
<link rel="stylesheet" href="assets/css/templatemo-stand-blog.css">
<link rel="stylesheet" href="assets/css/owl.css">
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%@include file="Header.jsp" %>
        <button onclick="goBack()"><<-Back</button>
        <c:set var="p" value="${requestScope.pos}"/>
        <section class="blog-posts grid-system">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="all-blog-posts">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="blog-post">
                                        <div class="blog-thumb">
                                            <img src="${p.image}" alt="Image" />
                                        </div>
                                        <div class="down-content">
                                            <span>${p.content}</span>
                                            <ul class="post-info">
                                            </ul>
                                            <p>${p.featuring} </p><br>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <%@include file="footer.jsp" %>
        <script>
            function goBack() {
                window.history.back();
            }
        </script>
    </body>
</html>
