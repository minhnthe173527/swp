
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Image Slider</title>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/style.css">
        <style>

            .service-sidebar{
                flex-basis: 22%;
                background-color: #f5f5f5; /* Set the background color for the sidebar */
                padding: 20px;
            }
            .service-content{
                flex-basis: 78%;
                padding: 20px;
            }
            a.active{
                color: green;
                font-weight: bold;
            }
            .sale-price{
                color: red;
                font-weight: bold;
            }

            /* Add any additional styles you need for your sidebar */
        </style>
    </head>
    <body  width = 70% >
        <%@include file="Header.jsp" %>
        <div class="container">

            <h6 class="text-heading">Service</h6>
            <div class="service-container">
                <!-- Sidebar -->
                <!--                <div class="col-md-3">
                                    <div class="row">-->
                <div class="service-sidebar">
                    <!-- Add your sidebar content here -->
                    <form action="servicelist">
                        <div class="form-group">
                            <label for="txtSearchValue">Search Value:</label>
                            <input type="text" class="form-control" id="txtSearchValue" name="txtSearchValue" value="${param.txtSearchValue}">
                        </div>
                        <button type="submit" class="btn btn-primary" name="btAction">Search</button>
                        <h4>Service Category:</h4>
                    </form>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <a href="servicelist?txtCateService=Medical%20Check-up" class="btn btn-secondary">Medical Check-up</a>
                        </li>
                        <li class="list-group-item">
                            <a href="servicelist?txtCateService=Medical%20Consultation" class="btn btn-secondary">Medical Consultation</a>
                        </li>
                        <li class="list-group-item">
                            <a href="servicelist?txtCateService=Health%20Care" class="btn btn-secondary">Health Care</a>
                        </li>
                    </ul>
                    <ul>
                        <li><a href="https://www.facebook.com/"><span class="fa fa-facebook"><i>Fan Page :</i></span></a></li>
                        <li>Hotline: <a href="tel:0855809219">0855.809.219 (8h - 22h)</a></li>
                        <li><a href="https://www.google.com/maps/d/u/0/viewer?mid=1qkoM0-zP-XrdOvBa6eVDVSQ4pyY&hl=en_US&ll=21.013391000000013%2C105.5266735&z=17"><span class="fa fa-dribbble"><i>Address :</i></span></a></li>
                    </ul>
                </div>
                <!--                    </div>
                                </div>-->

                <!-- Service Section -->
                <!--                <div class="col-md-9">-->
                <div class="service-content">
                    <c:set var="searchValue" value="${param.txtSearchValue}"/>

                    <div class="row">
                        <c:forEach items="${listsvp}" var="s">
                            <c:if test="${s.status == true}">
                                <div class="col-lg-3 col-md-3 col-sm-6 mb-4">
                                    <!-- Card for search results -->
                                    <div class="card h-100">
                                        <a href="detailservice?id=${s.serviceId}"><img class="card-img-top" src="images/${s.image}" alt=""></a>
                                        <div class="card-body">
                                            <h4 class="card-title">
                                                <a href="detailservice?id=${s.serviceId}">${s.name}</a>
                                            </h4>
                                            <h8>
                                                <del class="original-price">${s.price}$</del> <!-- Giá gốc -->
                                                <span class="sale-price">${s.price * 0.5}$</span> <!-- Giá sale (50% giá gốc) -->
                                            </h8>
                                        </div>
                                        <div class="card-footer">
                                            <a href="ReservationDetails?id=${s.serviceId}" class="btn btn-primary">Reservation</a>
                                            <a href="#" class="btn btn-secondary">Feedback</a>
                                        </div>
                                    </div>
                                </div>
                            </c:if>
                        </c:forEach>
                    </div>


                    <div class="row">
                        <c:forEach items="${listsv}" var="s">
                            <c:if test="${s.status == true}">
                                <div class="col-lg-3 col-md-4 col-sm-6 mb-4">
                                    <!-- Card for all services -->
                                    <div class="card h-100">
                                        <a href="detailservice?id=${s.serviceId}"><img class="card-img-top" src="images/${s.image}" alt=""></a>
                                        <div class="card-body">
                                            <h4 class="card-title">
                                                <a href="detailservice?id=${s.serviceId}">${s.name}</a>
                                            </h4>
                                            <del class="original-price">${s.price}$</del> <!-- Giá gốc -->
                                            <span class="sale-price">${s.price * 0.5}$</span> <!-- Giá sale (50% giá gốc) -->
                                        </div>
                                        <div class="card-footer">
                                            <a href="ReservationDetails?id=${s.serviceId}" class="btn btn-primary">Reservation</a>
                                            <a href="#" class="btn btn-secondary">Feedback</a>
                                        </div>
                                    </div>
                                </div>
                            </c:if>
                        </c:forEach>
                    </div>


                    <div class="row">
                        <c:forEach items="${listsvbycate}" var="s">
                            <c:if test="${s.status == true}">
                                <div class="col-lg-3 col-md-4 col-sm-6 mb-4">
                                    <!-- Card for all services -->
                                    <div class="card h-100">
                                        <a href="detailservice?id=${s.serviceId}"><img class="card-img-top" src="images/${s.image}" alt=""></a>
                                        <div class="card-body">
                                            <h4 class="card-title">
                                                <a href="detailservice?id=${s.serviceId}">${s.name}</a>
                                            </h4>
                                            <del class="original-price">${s.price}$</del> <!-- Giá gốc -->
                                            <span class="sale-price">${s.price * 0.5}$</span> <!-- Giá sale (50% giá gốc) -->
                                        </div>
                                        <div class="card-footer">
                                            <a href="ReservationDetails?id=${s.serviceId}" class="btn btn-primary">Reservation</a>
                                            <a href="#" class="btn btn-secondary">Feedback</a>
                                        </div>
                                    </div>
                                </div>
                            </c:if>
                        </c:forEach>
                    </div>


                    <c:forEach begin="1" end="${endP}" var="i">
                        <a class="${tag==i?"active":""}" href="servicelist?page=${i}">${i}</a>
                    </c:forEach>
                </div>
                <!--                </div>-->
            </div>
        </div>


        <!-- Link to Bootstrap JS and jQuery -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.3/dist/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    </div>
    <%@include file="footer.jsp" %>
</body>
</html>
