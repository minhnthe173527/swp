/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;

import Model.UserAccount;
import Model.UserAccountHistory;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author minh1 private int customerId; private String fullname; private String
 * email; private String password; private String mobile; private boolean
 * gender; private String status; private String image;
 */
public class UserDAO extends DBContext {

    public List<UserAccount> getAllAccount() {
        List<UserAccount> ls = new ArrayList<>();
        String sql = "SELECT * FROM [SWP].[dbo].[UserAccount]";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                UserAccount a = new UserAccount();
                a.setId(rs.getInt("id"));
                a.setName(rs.getString("name"));
                a.setEmail(rs.getString("email"));
                a.setPassword(rs.getString("password"));
                a.setMobile(rs.getString("mobile"));
                a.setGender(rs.getBoolean("gender"));
                a.setStatus(rs.getBoolean("status"));
                a.setAddress(rs.getString("address"));
                ls.add(a);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return ls;
    }

    public List<UserAccount> searchCustomerByName(String name) {
        List<UserAccount> ls = new ArrayList<>();
        String sql = "SELECT * FROM [SWP].[dbo].[UserAccount] where FullName like ? or Email like ? or Mobile like ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + name + "%");
            st.setString(2, "%" + name + "%");
            st.setString(3, "%" + name + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                UserAccount a = new UserAccount();
                a.setId(rs.getInt("id"));
                a.setName(rs.getString("name"));
                a.setEmail(rs.getString("email"));
                a.setPassword(rs.getString("password"));
                a.setMobile(rs.getString("mobile"));
                a.setGender(rs.getBoolean("gender"));
                a.setStatus(rs.getBoolean("status"));
                a.setAddress(rs.getString("address"));

                ls.add(a);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return ls;
    }

    public List<UserAccount> searchCustomerByStatus(boolean status) {
        List<UserAccount> ls = new ArrayList<>();
        String sql = "SELECT * FROM [SWP].[dbo].[UserAccount] where Status like ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setBoolean(1, status);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                UserAccount a = new UserAccount();
                a.setId(rs.getInt("id"));
                a.setName(rs.getString("name"));
                a.setEmail(rs.getString("email"));
                a.setPassword(rs.getString("password"));
                a.setMobile(rs.getString("mobile"));
                a.setGender(rs.getBoolean("gender"));
                a.setStatus(rs.getBoolean("status"));
                a.setAddress(rs.getString("address"));

                ls.add(a);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return ls;
    }

    public void updateStatusCustomer(int id, boolean newStatus) {
        String sql = "UPDATE [dbo].[UserAccount]\n"
                + "  SET [Status] = ?\n"
                + " WHERE [ID] = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setBoolean(1, newStatus);
            st.setInt(2, id);
            int cc = st.executeUpdate();
            if (cc > 0) {
                System.out.println("done");
            } else {
                System.out.println("ngu");
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public boolean getStatusById(int id) {
        String sql = "SELECT [Status] FROM [dbo].[UserAccount] WHERE [ID] = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getBoolean("Status");
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return false;
    }

    public List<UserAccount> sortAccount(String colName) {
        List<UserAccount> ls = new ArrayList<>();
        String sql = "SELECT * FROM [SWP].[dbo].[UserAccount] order by " + colName + " ASC";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                UserAccount a = new UserAccount();
                a.setId(rs.getInt("id"));
                a.setName(rs.getString("name"));
                a.setEmail(rs.getString("email"));
                a.setPassword(rs.getString("password"));
                a.setMobile(rs.getString("mobile"));
                a.setGender(rs.getBoolean("gender"));
                a.setStatus(rs.getBoolean("status"));
                a.setAddress(rs.getString("address"));
                ls.add(a);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return ls;
    }

    public UserAccount getUserById(String id) {
        String sql = "SELECT * FROM [SWP].[dbo].[UserAccount] where ID =?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                UserAccount a = new UserAccount();
                a.setId(rs.getInt("id"));
                a.setName(rs.getString("name"));
                a.setEmail(rs.getString("email"));
                a.setPassword(rs.getString("password"));
                a.setMobile(rs.getString("mobile"));
                a.setGender(rs.getBoolean("gender"));
                a.setStatus(rs.getBoolean("status"));
                a.setAddress(rs.getString("address"));
                return a;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }

        return null;
    }

    public UserAccount updateUserInfo(String id, String name, String password, String mobile, boolean gender, String address) {
        String sql = "UPDATE UserAccount SET [Name] = ?, [Password] = ?, [Mobile] = ?, [Gender] = ?,  [Address] = ? WHERE [ID] = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            st.setString(1, name);
            st.setString(2, password);
            st.setString(3, mobile);
            st.setBoolean(4, gender);
            st.setString(5, address);
            st.setString(6, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                UserAccount a = new UserAccount();
                a.setId(rs.getInt("id"));
                a.setName(rs.getString("name"));
                a.setEmail(rs.getString("email"));
                a.setPassword(rs.getString("password"));
                a.setMobile(rs.getString("mobile"));
                a.setGender(rs.getBoolean("gender"));
                a.setStatus(rs.getBoolean("status"));
                a.setAddress(rs.getString("address"));
                return a;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }

        return null;
    }

    public List<UserAccountHistory> getHistory(String id) {
        String sql = "SELECT [HistoryID]\n"
                + "      ,[UserID]\n"
                + "      ,[Name]\n"
                + "      ,[Email]\n"
                + "      ,[Password]\n"
                + "      ,[Mobile]\n"
                + "      ,[Gender]\n"
                + "      ,[Status]\n"
                + "      ,[Address]\n"
                + "      ,[ModifiedDate]\n"
                + "  FROM [SWP].[dbo].[UserAccountHistory] where UserID = ?";
        List<UserAccountHistory> ls = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            
            while (rs.next()) {
                UserAccountHistory u = new UserAccountHistory();
                u.setHistoryID(rs.getInt("historyID"));
                u.setUserID(rs.getInt("userID"));
                u.setName(rs.getString("name"));
                u.setEmail(rs.getString("email"));
                u.setMobile(rs.getString("mobile"));
                u.setGender(rs.getBoolean("gender"));
                u.setStatus(rs.getBoolean("status"));
                u.setAddress(rs.getString("address"));
                u.setModifiedDate(rs.getDate("modifiedDate"));
                ls.add(u);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return ls;
    }

    public static void main(String[] args) {
        UserDAO cd = new UserDAO();
        List<UserAccountHistory> ls = cd.getHistory("1");
        for (UserAccountHistory l : ls) {
            System.out.println(l.getModifiedDate());
        }
    }
}
