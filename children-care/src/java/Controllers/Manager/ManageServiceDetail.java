/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controllers.Manager;

import DAO.ManageSeviceDAO;
import DAO.ServiceCategoryDAO;
import Model.ServiceCategory;
import Model.ServiceList;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;

/**
 *
 * @author ADMIN
 */
@WebServlet(name = "ManageServiceDetail", urlPatterns = {"/manageservicedetail"})
public class ManageServiceDetail extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        int svId = Integer.parseInt(request.getParameter("svId"));
        //--------------
        ManageSeviceDAO dao = new ManageSeviceDAO();
        ServiceCategoryDAO catedao = new ServiceCategoryDAO();
        if (request.getParameter("btnSave") != null) {
            String svName = request.getParameter("svName");
            String svDescription = request.getParameter("svDescription");
            double svPrice = Double.parseDouble(request.getParameter("svPrice"));
            int svCategory = Integer.parseInt(request.getParameter("svCategory"));
            dao.updateServicebyId(svId, svName, svDescription, svPrice, svCategory);
            request.setAttribute("mess", "Update Successfully!");
        }
        ServiceList service;
        service = dao.getServicebyId(svId);
        List<ServiceCategory> svCate;
        svCate = catedao.getAllServiceCategory();
        int totalcate = dao.getQuantityServiceCategory();
        request.setAttribute("totalcate", totalcate);
        request.setAttribute("service", service);
        request.setAttribute("svCate", svCate);
        request.getRequestDispatcher("ManageServiceDetail.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
