/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;

import Model.Cart;
import Model.ServiceCategory;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author SANG
 */
public class ReservationDao extends DBContext {

    public List<Cart> getCartByCusID(int CusID) {
        List<Cart> ListCart = new ArrayList<>();
        ServiceDAO svd = new ServiceDAO();
        try {
            String sql = "select * from Reservation where customerID = " + CusID + ";";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Cart cart = new Cart();
                cart.setCustomerID(rs.getInt(1));
                cart.setService(svd.getServiceById(rs.getInt(2)+""));
                cart.setQuantity(rs.getInt(3));
                cart.setNumberPerson(rs.getInt(4));
                ListCart.add(cart);
            }
        } catch (Exception e) {
        }
        return ListCart;
    }

    public Cart getCartByServiceAndCustomer(int ServiceID, int CustomerID) {
        ServiceDAO svd = new ServiceDAO();
        try {
            String sql = "select * from Reservation where customerID = " + CustomerID + " and ServiceID = " + ServiceID + ";";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Cart cart = new Cart();
                cart.setCustomerID(rs.getInt(1));
                cart.setService(svd.getServiceById(rs.getInt(2)+""));
                cart.setQuantity(rs.getInt(3));
                cart.setNumberPerson(rs.getInt(4));
                return cart;
            }
        } catch (Exception e) {
        }
        return null;
    }
    public boolean ChangeNumberPerson(int ServiceID, int CustomerID, int NumberPerson) {
        if(NumberPerson==0){
        return false;
        }
        try {
            String sql = "Update Reservation set NumberofPerson = " + NumberPerson + " where customerID = " + CustomerID + " and ServiceID = " + ServiceID + ";";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean ChangeQuantity(int ServiceID, int CustomerID, int quantity) {
        if(quantity==0){
        return false;
        }
        try {
            String sql = "Update Reservation set Quantity = " + quantity + " where customerID = " + CustomerID + " and ServiceID = " + ServiceID + ";";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    public static void main(String[] args) {
        ReservationDao cd = new ReservationDao();
         boolean c = cd.ChangeQuantity(1, 1, 5);
         System.out.println(c);
    }

    public boolean AddToCart(int ServiceID, int CustomerID) {
        try {
            String sql = "insert into Reservation values\n"
                    + "(" + CustomerID + ", " + ServiceID + ", 1, 1);";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    public boolean DeleteCart(int ServiceID, int CustomerID){
        try {
            String sql = "delete Reservation where CustomerID = "+CustomerID+" and ServiceID = "+ServiceID+";";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
