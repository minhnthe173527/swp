/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controllers.Public;

import DAO.ServiceCategoryDAO;
import DAO.ReservationDao;
import DAO.ServiceDAO;
import Model.Account;
import Model.Cart;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.HashMap;
import Model.Service;
import Model.ServiceCategory;
import jakarta.servlet.annotation.WebServlet;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name="ReservationDetailsController", urlPatterns={"/ReservationDetails"})
public class ReservationDetailsController extends HttpServlet {
 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ReservationDao cd = new ReservationDao();
        ServiceDAO svd = new ServiceDAO();
        HttpSession session = request.getSession();

        if(session.getAttribute("account") == null){
            response.sendRedirect("login");
            return;
        }
        Account Cus = (Account) session.getAttribute("account");
        int CusID = Cus.getId();
        String IDservicestr = request.getParameter("id");
        String upservicestr = request.getParameter("upService");
        String upNumberPersonstr = request.getParameter("upNumberPerson");
        String checkstr = request.getParameter("check");
        boolean check = false;
        if (checkstr != null) {
            check = Boolean.parseBoolean(checkstr.trim());
        }
        String servicedeletestr = request.getParameter("servicedelete");
        if (upservicestr != null) {
            int upservices = Integer.parseInt(upservicestr);
            Cart cart = cd.getCartByServiceAndCustomer(upservices, CusID);
            if (check) {
                cd.ChangeQuantity(upservices, CusID, cart.getQuantity() + 1);
            } else {
                cd.ChangeQuantity(upservices, CusID, cart.getQuantity() - 1);
            }
        }
        if (upNumberPersonstr != null) {
            int upNumberPerson = Integer.parseInt(upNumberPersonstr);
            Cart cart = cd.getCartByServiceAndCustomer(upNumberPerson, CusID);
            if (check) {
                cd.ChangeNumberPerson(upNumberPerson, CusID, cart.getNumberPerson() + 1);
            } else {
                cd.ChangeNumberPerson(upNumberPerson, CusID, cart.getNumberPerson() - 1);
            }
        }
        if (servicedeletestr != null) {
            int servicedelete = Integer.parseInt(servicedeletestr);
            cd.DeleteCart(servicedelete, CusID);
        }
        List<Cart> listcart = new ArrayList<>();
        if (IDservicestr == null) {
            listcart = cd.getCartByCusID(CusID);
            request.setAttribute("listcart", listcart);
        } else {
            int IDservice = Integer.parseInt(IDservicestr);
            Cart cart = cd.getCartByServiceAndCustomer(IDservice, CusID);
            if (cart == null) {
                cd.AddToCart(IDservice, CusID);
                listcart = cd.getCartByCusID(CusID);
                request.setAttribute("listcart", listcart);
            } else {
                int quatity = cart.getQuantity() + 1;
                cd.ChangeQuantity(IDservice, CusID, quatity);
                listcart = cd.getCartByCusID(CusID);
                request.setAttribute("listcart", listcart);
            }
        }
        listcart = cd.getCartByCusID(CusID);
        request.setAttribute("listcart", listcart);
        double Total = getTotals(listcart);
        request.setAttribute("totals", Total);
        ServiceCategoryDAO catedao = new ServiceCategoryDAO();
        List<ServiceCategory> listcategory = catedao.getAllServiceCategory();
        request.setAttribute("listcategory", listcategory);
        request.getRequestDispatcher("ReservationDetails.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public double getTotals(List<Cart> lc) {
        double total = 0;
        for (Cart cart : lc) {
            total = total + (cart.getService().getPrice() * cart.getNumberPerson() * cart.getQuantity());
        }
        return total;
    }
}
