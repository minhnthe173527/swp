/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controllers.Public;

import DAO.ServiceListDAO;
import Model.ServiceList;
import jakarta.servlet.RequestDispatcher;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author ADMIN
 */
@WebServlet(name = "ServiceController", urlPatterns = {"/servicelist"})
public class ServiceController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String searchValue = request.getParameter("txtSearchValue");
        String cateService = request.getParameter("txtCateService");
        String indexPage = request.getParameter("page");
        if (indexPage == null) {
            indexPage = "1";
        }
        int index = Integer.parseInt(indexPage);
        //call DAO
        ServiceListDAO dao = new ServiceListDAO();
        List<ServiceList> sv;
        //<--- pagging -->
        int count;
        int endPage;
//        sv = dao.getAllService();
        if (searchValue != null) {
            sv = dao.SearchService(searchValue);
            request.setAttribute("listsv", sv);
        } else if (cateService != null) {
            sv = dao.ListServiceByCate(cateService);
            request.setAttribute("listsvbycate", sv);
        } else {
            count = dao.getQuantityService();
            endPage = count / 4;
            if (count % 4 != 0) {
                endPage++;
            }
            sv = dao.paggingService(index);
            request.setAttribute("endP", endPage);
            request.setAttribute("listsvp", sv);
        }
        request.setAttribute("tag", index);
        request.getRequestDispatcher("ServiceList.jsp").forward(request, response);
//        if (searchValue == "") {
//            response.sendRedirect("Home.jsp");
//        } else {
//            sv = dao.SearchService(searchValue);
//            request.setAttribute("listsv", sv);
//            request.getRequestDispatcher("Home.jsp").forward(request, response);
//        }
    }

    public static void main(String[] args) {
        ServiceListDAO dao = new ServiceListDAO();
        List<ServiceList> sv = dao.SearchService("");
        for (ServiceList o : sv) {
            System.out.println(o);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
