/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;


/**
 *
 * @author ADMIN
 */

public class Slider {
    private int sliderId;
    private String title;
    private String image;
    private String backlink;
    private String notes;
    private boolean Status;

    public Slider(int sliderId, String title, String image, String backlink, String notes, boolean Status) {
        this.sliderId = sliderId;
        this.title = title;
        this.image = image;
        this.backlink = backlink;
        this.notes = notes;
        this.Status = Status;
    }

    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }
    
    

    public Slider() {
    }

    public Slider(int sliderId, String title, String image, String backlink, String notes) {
        this.sliderId = sliderId;
        this.title = title;
        this.image = image;
        this.backlink = backlink;
        this.notes = notes;
    }

    public int getSliderId() {
        return sliderId;
    }

    public void setSliderId(int sliderId) {
        this.sliderId = sliderId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getBacklink() {
        return backlink;
    }

    public void setBacklink(String backlink) {
        this.backlink = backlink;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
    
}
