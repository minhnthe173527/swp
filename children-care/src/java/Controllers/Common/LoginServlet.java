/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controllers.Common;

import DAO.AccountDAO;
import Model.Account;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/**
 *
 * @author Lenovo
 */
public class LoginServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute("account");
        //check if user logged in
        if (account != null) {
            if ("customer".equals(account.getRole().toLowerCase())) {
                response.sendRedirect("home");
            }
            if ("staff".equals(account.getRole().toLowerCase()) ) {
                request.getRequestDispatcher("StaffDashboard.jsp").forward(request, response);
            }
            if ("manager".equals(account.getRole().toLowerCase())) {
                request.getRequestDispatcher("manageservicelist").forward(request, response);
            }
            if ("admin".equals(account.getRole().toLowerCase())) {
                request.getRequestDispatcher("AdminDashboard.jsp").forward(request, response);
            }
            return;
        }
        String email = (String) request.getParameter("email");
        String password = (String) request.getParameter("password");
        //forward to login page for input account information
        if (email == null) {
            request.getRequestDispatcher("Login.jsp").forward(request, response);
            return;
        }
        AccountDAO dao = new AccountDAO();
        Account userInfo = dao.checkLogin(new Account(email, password));
        //check user info is exist in database
        if (userInfo != null) {
            session.setAttribute("account", userInfo);
            //verify user login
            request.getRequestDispatcher("login").forward(request, response);
        } else {
            //return a message ìf username or password is incorrect
            request.setAttribute("msg", "Invalid username or password");
            request.getRequestDispatcher("Login.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
