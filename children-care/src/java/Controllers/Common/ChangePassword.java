/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controllers.Common;

import DAO.AccountDAO;
import Model.Account;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/**
 *
 * @author asus
 */
public class ChangePassword extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.doGet(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        String oldpass = request.getParameter("oldpass");
        String newpass = request.getParameter("newpass");
        String renewpass = request.getParameter("renewpass");
        Account u = (Account) session.getAttribute("account");
        AccountDAO dao = new AccountDAO();
//        Account userInfo = dao.getAccount(new Account(u.getEmail()));
//        
        if (!oldpass.equals(u.getPassword())) {
            request.setAttribute("mess", "Old Password incorrect!!!");
            request.getRequestDispatcher("ChangePassword.jsp").forward(request, response);
        } else if (!newpass.equals(renewpass)) {
            request.setAttribute("mess", "Confirm password not match with new password");
            request.getRequestDispatcher("ChangePassword.jsp").forward(request, response);
        } else {
            if ("customer".equals(u.getRole())) {
                dao.changePassword(u.getEmail(), newpass, "UserAccount");
            } else {
                dao.changePassword(u.getEmail(), newpass, "EmployeeAccount");
            }
            session.invalidate();
            response.sendRedirect(request.getContextPath() + "/Login.jsp");
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
