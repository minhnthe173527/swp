/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controllers.Manager;

import DAO.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 *
 * @author minh1
 */
@WebServlet(name = "UpdateCustomer", urlPatterns = {"/updatecustomer"})
public class UpdateCustomer extends HttpServlet {

        @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getParameter("updateStatus") != null) {
                int CusId = Integer.parseInt(request.getParameter("updateStatus"));
                UserDAO cd = new UserDAO();
                boolean newStatus = !cd.getStatusById(CusId);
                cd.updateStatusCustomer(CusId, newStatus);
                request.getRequestDispatcher("manageruser").forward(request, response);
                }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
