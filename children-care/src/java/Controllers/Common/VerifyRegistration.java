/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controllers.Common;

import DAO.AccountDAO;
import Model.Account;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Date;

/**
 *
 * @author Lenovo
 */

public class VerifyRegistration extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String encodedData = request.getParameter("data");
        //return error for empty data
        if (encodedData == null) {
            out.println("Error! Click <a href='signup'>here</a>");
            return;
        }
        byte[] decodedBytes = Base64.getDecoder().decode(encodedData);
        String decodedData = new String(decodedBytes, StandardCharsets.UTF_8);
        AccountDAO dao = new AccountDAO();
        //get user information from decoded data
        String[] data = decodedData.split("\\|");
        //check url expired
        long currentTime = new Date().getTime()/1000;
        if(Integer.parseInt(data[6])<currentTime){
            out.print("Expired! Please sign up again <a href='signup'>here</a>");
            return;
        }
        //check exist
        if(dao.checkExist(data[0])){
            out.print("Your account has been signed successfully! Click <a href=login>here</a> to login");
            return;
        }
        boolean gender = true;
        if ("female".equals(data[3])) {
            gender = false;
        }
        Account newAccount = new Account(data[1], data[0], data[2], data[4], gender, data[5]);
        boolean result = dao.userRegister(newAccount);
        if (result) {
            out.println("Sign up succesfully! Click <a href=login>here</a> to login");
        } else {
            out.print("Error! Click <a href='signup'>here</a> to sign up again");
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
