/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controllers.Common;

import DAO.AccountDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.Base64;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;

/**
 *
 * @author Lenovo
 */
public class GetResetUrl extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();
            AccountDAO dao = new AccountDAO();
            String email = request.getParameter("email");
            //forward to reset page
            if (email == null) {
                request.getRequestDispatcher("Reset.jsp").forward(request, response);
                return;
            }
            //check user email is valid
            boolean isValid = dao.checkExist(email);
            if (!isValid) {
                request.setAttribute("msg", "Your account does not exist");
                request.getRequestDispatcher("Reset.jsp").forward(request, response);
                return;
            }
            //create reset link
            long duration = 300;
            long expiredTime = new Date().getTime() / 1000 + duration;
            String data = email + "|" + expiredTime;
            String encodedData = Base64.getEncoder().encodeToString(data.getBytes());
            String url = "http://localhost:9999/ChildrenCare/resetpw?data=" + encodedData;//create url
            //set up content
            String content = "<html><body>";
            content += "<p>Click here to reset password</p>";
            content += "<a href='" + url + "'><button style='background-color: Blue; color: white; padding: 10px 20px; border: none; cursor: pointer;'>Reset</button></a>";
            content += "<p>Your url will be expired in 5 minutes</p>";
            content += "</body></html>";
            //send email
            dao.sendEmail(email, content);
            out.println("<script>");
            out.println("var confirmation = confirm('We have sent you an email to reset your password, check email?')");
            out.println("if (confirmation) {");
            out.println("window.location.href = 'https://www.gmail.com';");
            out.println("} else {");
            out.println("window.location.href = 'login';");
            out.println("}");
            out.println("</script>");
        } catch (MessagingException ex) {
            Logger.getLogger(GetResetUrl.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
