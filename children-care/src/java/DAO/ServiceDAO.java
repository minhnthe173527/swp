/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;


import Model.Service;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author minh1
 *
 * private int serviceId; private String serviceName; private String
 * description; private double price; private int categoryId; private String
 * image;
 */
public class ServiceDAO extends DBContext {

    public List<Service> getAllService() {
        List<Service> ls = new ArrayList<>();
        String sql = "SELECT [ServiceID]\n"
                + "      ,[ServiceName]\n"
                + "      ,[Description]\n"
                + "      ,[Price]\n"
                + "      ,[CategoryID]\n"
                + "      ,[Image]\n"
                + "  FROM [dbo].[Service]\n";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Service s = new Service();
                s.setServiceId(rs.getInt("serviceId"));
                s.setServiceName(rs.getString("serviceName"));
                s.setDescription(rs.getString("description"));
                s.setPrice(rs.getDouble("price"));
                s.setCategoryId(rs.getInt("categoryId"));
                s.setImage(rs.getString("image"));
                ls.add(s);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return ls;
    }

    public Service getServiceById(String id) {
        String sql = "SELECT [ServiceID]\n"
                + "      ,[ServiceName]\n"
                + "      ,[Description]\n"
                + "      ,[Price]\n"
                + "      ,[CategoryID]\n"
                + "      ,[Image]\n"
                + "  FROM [dbo].[Service] where ServiceID = ?\n";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Service s = new Service();
                s.setServiceId(rs.getInt("serviceId"));
                s.setServiceName(rs.getString("serviceName"));
                s.setDescription(rs.getString("description"));
                s.setPrice(rs.getDouble("price"));
                s.setCategoryId(rs.getInt("categoryId"));
                s.setImage(rs.getString("image"));
                return s;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public List<Service> searchServiceByName(String name) {
        ArrayList<Service> ls = new ArrayList<>();
        String sql = "SELECT [ServiceID]\n"
                + "      ,[ServiceName]\n"
                + "      ,[Description]\n"
                + "      ,[Price]\n"
                + "      ,[CategoryID]\n"
                + "      ,[Image]\n"
                + "  FROM [dbo].[Service]\n where [ServiceName] like ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + name + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Service s = new Service();
                s.setServiceId(rs.getInt("serviceId"));
                s.setServiceName(rs.getString("serviceName"));
                s.setDescription(rs.getString("description"));
                s.setPrice(rs.getDouble("price"));
                s.setCategoryId(rs.getInt("categoryId"));
                s.setImage(rs.getString("image"));
                ls.add(s);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return ls;
    }

    public List<Service> getServiceByCategoryName(String cname) {
        List<Service> ls = new ArrayList<>();
        String sql = "SELECT s.ServiceID, s.ServiceName, s.Description, s.Price, s.Image\n"
                + "                FROM Service s\n"
                + "                INNER JOIN ServiceCategory sc ON s.CategoryID = sc.CID\n"
                + "                WHERE sc.CName like ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, cname);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Service s = new Service();
                s.setServiceId(rs.getInt("serviceId"));
                s.setServiceName(rs.getString("serviceName"));
                s.setDescription(rs.getString("description"));
                s.setPrice(rs.getDouble("price"));
                s.setImage(rs.getString("image"));
                ls.add(s);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return ls;
    }

    public static void main(String[] args) {
        ServiceDAO d = new ServiceDAO();
        List<Service> ls = d.getServiceByCategoryName("Dentomaxillofacial");
        for (Service a : ls) {
            System.out.println(a);
        }
    }
}
