/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controllers.Staff;

import DAO.BlogDAO;
import Model.Blog;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;

/**
 *
 * @author asus
 */
public class blog extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        BlogDAO dao = new BlogDAO();
        int totalblog = dao.getNumberBlog();
        int numberPage = (int) Math.ceil((double) totalblog / 4);
        int index;
        String currentPage = request.getParameter("index");
        if (currentPage == null) {
            index = 1;
        } else {
            index = Integer.parseInt(currentPage);
        }

        ArrayList<Blog> list = dao.getBlog(index);
        ArrayList<Blog> recent = dao.getRecentBlog();
        request.setAttribute("numberPage", numberPage);
        request.setAttribute("recent", recent);
        request.setAttribute("blog", list);
        request.getRequestDispatcher("Blog.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String search = request.getParameter("search");
        BlogDAO dao = new BlogDAO();
        ArrayList<Blog> blog = dao.searchBlog(search);
        int totalblog = dao.getNumberBlog();
        int numberPage = (int) Math.ceil((double) totalblog / 4);
        int index;
        String currentPage = request.getParameter("index");
        if (currentPage == null) {
            index = 1;
        } else {
            index = Integer.parseInt(currentPage);
        }
        request.setAttribute("blog", blog);
        ArrayList<Blog> recent = dao.getRecentBlog();
        request.setAttribute("numberPage", numberPage);
        request.setAttribute("recent", recent);
        request.getRequestDispatcher("Blog.jsp").forward(request, response);

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
