/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;

import Model.Feedback;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author SANG
 */
public class FeedBackDAO extends DBContext {

    public List<Feedback> getAllFeedbackByCondtion(String status, String rate, String searchfeedbackcontent, String searchfullname, String searchservice, int index, int pagesize) {
        List<Feedback> list = new ArrayList<>();
        ServiceDAO svd = new ServiceDAO();
        CustomerAccountDAO cad = new CustomerAccountDAO();
        try {
            String sql;

            sql = "select * from Feedback f where ";
            if (!status.isEmpty()) {
                sql += " f.[Status] =" + status + " and ";
            }
            sql += "f.Content like '%" + searchfeedbackcontent + "%' and f.[rate_star] like '%" + rate + "%'\n"
                    + "ORDER BY f.FeedbackID\n"
                    + "OFFSET " + (index - 1) * pagesize + " ROWS\n"
                    + "FETCH NEXT " + pagesize + " ROWS ONLY;";
            System.out.println(sql);
            PreparedStatement stm = connection.prepareStatement(sql);

            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                /*
                [FeedbackID]
      ,[CustomerID]
      ,[Content]
      ,[PublishedDate]
      ,[ServiceID]
      ,[FeedbackIMG]
      ,[Status]
      ,[rate_star]*/
                Feedback sl = new Feedback();
                sl.setFeedbackId(rs.getInt(1));
                sl.setCustomerId(rs.getInt(2));
                sl.setContent(rs.getString(3));
                sl.setPublishedDate(rs.getString(4));
                sl.setServiceId(rs.getInt(5));
                sl.setImg(rs.getString(6));
                int statusdb = rs.getInt(7);
                if (statusdb == 1) {
                    sl.setStatus(true);
                } else {
                    sl.setStatus(false);
                }
                sl.setRate(rs.getFloat(8));
                sl.setService(svd.getServiceById(sl.getServiceId() + ""));
                sl.setCustomerAcc(cad.getCustomerAcc(sl.getCustomerId()));
                if (sl.getCustomerAcc().getFullname().contains(searchfullname.trim()) && sl.getService().getServiceName().contains(searchservice.trim())) {
                    list.add(sl);
                }
            }
            stm.close();
        } catch (SQLException ex) {

        }
        return list;
    }

    public boolean addAnewFeedBack(int CustomerID, String Content, String ServiceID, String FeedbackIMG, String rate_star) {
        SimpleDateFormat frm = new SimpleDateFormat("yyyy-MM-dd");
        String PublishedDate = frm.format(new Date());
        try {
            String sql = "insert into Feedback ([CustomerID]\n"
                    + "      ,[Content]\n"
                    + "      ,[PublishedDate]\n"
                    + "      ,[ServiceID]\n"
                    + "      ,[FeedbackIMG]\n"
                    + "      ,[Status]\n"
                    + "      ,[rate_star]) values\n"
                    + "(" + CustomerID + ",'" + Content + "', '" + PublishedDate + "', " + ServiceID + ", '" + FeedbackIMG + "', 1, " + rate_star + ")";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.executeUpdate();
            return true;
        } catch (Exception e) {
        }
        return false;
    }
    

    public Feedback getFeedbackByID(String ID) {
        ServiceDAO svd = new ServiceDAO();
        CustomerAccountDAO cad = new CustomerAccountDAO();
        try {
            String sql = "  select * from [Feedback] f where f.FeedbackID = " + ID;
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Feedback fb = new Feedback();
                fb.setFeedbackId(rs.getInt(1));
                fb.setCustomerId(rs.getInt(2));
                fb.setContent(rs.getString(3));
                fb.setPublishedDate(rs.getString(4));
                fb.setServiceId(rs.getInt(5));
                fb.setImg(rs.getString(6));
                int statusdb = rs.getInt(7);
                if (statusdb == 1) {
                    fb.setStatus(true);
                } else {
                    fb.setStatus(false);
                }
                fb.setRate(rs.getFloat(8));
                fb.setService(svd.getServiceById(fb.getServiceId() + ""));
                fb.setCustomerAcc(cad.getCustomerAcc(fb.getCustomerId()));
                return fb;
            }
        } catch (Exception e) {
        }
        return null;
    }

    public boolean changeStatusfeedback(String ID , String status) {
        try {
            String sql = "update [Feedback] \n"
                    + "  set [Status] = "+status+" where [FeedbackID] = " + ID;
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.executeUpdate();
            return true;
        } catch (Exception e) {
        }
        return false;
    }

    public static void main(String[] args) {
        FeedBackDAO fd = new FeedBackDAO();
        System.out.println(fd.getFeedbackByID("1").getContent());
    }
}
