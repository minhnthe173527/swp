<%-- 
    Document   : searchservicebyname
    Created on : Sep 21, 2023, 10:41:07 AM
    Author     : minh1
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link rel="stylesheet" href="css/style.css">
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
       <%@include file="Header.jsp" %>
       <button onclick="goBack()"><<-Back</button>
       <div  style="text-align:  center"><h3 style="color: red">${requestScope.error}</h3></div>
       
       <div class="service-container">
            <div class="row">
                <c:forEach items="${requestScope.ser}" var="s">
                    <div class="service-item col-sm-4">
                        <a href="detailservice?id=${s.serviceId}">
                            <img class="service-image" src="images/${s.image}" alt="Image" />
                        </a>
                        <h4>${s.serviceName}</h4>
                    </div>
                </c:forEach>
            </div>
        </div>
       
       
       <%@include file="footer.jsp" %>
       <script>
        // Hàm goBack() sử dụng JavaScript để quay lại trang trước đó trong lịch sử duyệt
        function goBack() {
            window.history.back();
        }
    </script>
    </body>
</html>
