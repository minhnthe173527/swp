/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;


/**
 *
 * @author ADMIN
 */

//int CustomerID;
//    String Fullname;
//    String Email;
//    String Password;
//    String Mobile;
//    int Gender;
//    int Status;
//    String Address;

public class CustomerAcc {
    private int customerId;
    private String fullname;
    private String email;
    private String password;
    private String mobile;
    private boolean gender;
    private String status;
    private String Address;
    
    
    public CustomerAcc() {
    }

    public CustomerAcc(int customerId, String fullname, String email, String password, String mobile, boolean gender, String status) {
        this.customerId = customerId;
        this.fullname = fullname;
        this.email = email;
        this.password = password;
        this.mobile = mobile;
        this.gender = gender;
        this.status = status;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public boolean isGender() {
        return gender;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
}