<%-- 
    Document   : ResavationDetails
    Created on : Sep 16, 2023, 11:38:06 PM
    Author     : SANG
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Giỏ Hàng1</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <style>
            /* CSS cho layout chung */
            body {
                font-family: Arial, sans-serif;
                margin: 0;
                padding: 0;
                justify-content: center;
                background-color: #f4f4f4;
            }

            .sider {
                width: 20%;
                padding: 20px;
                background-color: #ece9e9;
                color: #fff;
            }

            .main-content {
                width: 80%;
                padding: 20px;
            }

            /* CSS cho giỏ hàng */
            .cart {
                margin: 20px;
                padding: 20px;
                border: 1px solid #ccc;
                border-radius: 5px;
                background-color: #fff;
            }

            .cart h2 {
                font-size: 1.5rem;
                margin-bottom: 20px;
            }

            .cart-item {
                display: flex;
                margin-bottom: 20px;
            }

            .cart-item img {
                max-width: 100px;
                max-height: 100px;
                margin-right: 20px;
                border-radius: 5px;
            }

            .cart-item-details {
                flex: 1;
            }

            .cart-item h3 {
                margin: 0;
            }

            .cart-item p {
                margin: 5px 0;
            }

            /* CSS cho sidebar */
            .sidebar{
                flex-basis: 22%;
                background-color: #e0f2f1;
                ; /* Set the background color for the sidebar */
                padding: 20px;
            }

            .sidebar h2 {
                font-size: 1.5rem;
                margin-bottom: 10px;
            }

            .category-list {
                list-style: none;
                padding: 0;
            }

            .category-list li {
                margin-bottom: 10px;
            }

            /* CSS cho ô tìm kiếm */
            .search-box {
                margin-top: 20px;
                padding: 20px;
                border: 1px solid #ccc;
                border-radius: 5px;
                background-color: #fff;
            }

            .search-box h2 {
                font-size: 3px;
                margin-bottom: 10px;
            }

            .search-input {
                width: 100%;
                padding: 10px;
                border: 1px solid #ccc;
                border-radius: 5px;
            }

            .search-button {
                background-color: #007bff;
                color: #fff;
                border: none;
                border-radius: 5px;
                padding: 5px 20px;
                cursor: pointer;
            }
            .cart_quantity_button a {
                background:#F0F0E9;
                color: #696763;
                display: inline-block;
                font-size: 16px;
                height: 34px;
                overflow: hidden;
                text-align: center;
                width: 35px;
                float: left;
            }
            .cart_quantity_input {
                color: #696763;
                float: left;
                font-size: 16px;
                text-align: center;
                font-family: 'Roboto',sans-serif;

            }

        </style>
    </head>
    <body>
        <%@include file="Header.jsp" %>
        <div>
            <button onclick="goBack()"><<-Back</button>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <div class="sidebar">
                    <!-- Add your sidebar content here -->
                    <form action="servicelist">
                        <div class="form-group">
                            <label for="txtSearchValue">Search Value:</label>
                            <input type="text" class="form-control" id="txtSearchValue" name="txtSearchValue" value="${param.txtSearchValue}">
                        </div>
                        <button type="submit" class="btn btn-primary" name="btAction">Search</button>
                        <h4>Service Category:</h4>
                    </form>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <a href="servicelist?txtCateService=Medical%20Check-up" class="btn btn-secondary">Medical Check-up</a>
                        </li>
                        <li class="list-group-item">
                            <a href="servicelist?txtCateService=Medical%20Consultation" class="btn btn-secondary">Medical Consultation</a>
                        </li>
                        <li class="list-group-item">
                            <a href="servicelist?txtCateService=Health%20Care" class="btn btn-secondary">Health Care</a>
                        </li>
                    </ul>
                    <ul>
                        <li><a href="https://www.facebook.com/"><span class="fa fa-facebook"><i>Fan Page :</i></span></a></li>
                        <li>Hotline: <a href="tel:0855809219">0855.809.219 (8h - 22h)</a></li>
                        <li><a href="https://www.google.com/maps/d/u/0/viewer?mid=1qkoM0-zP-XrdOvBa6eVDVSQ4pyY&hl=en_US&ll=21.013391000000013%2C105.5266735&z=17"><span class="fa fa-dribbble"><i>Address :</i></span></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-9">
                <h1>Reservation Details</h1>
                <form action="ReservationContact" method="get">
                    <div class="table-responsive cart_info">
                        <table class="table table-condensed">
                            <thead>
                                <tr class="cart_menu">
                                    <td class="image">Selected</td>
                                    <td class="quantity">ID</td>
                                    <td class="description">Title</td>
                                    <td class="price">Price</td>
                                    <td class="quantity">Quantity</td>
                                    <td class="quantity">Number of person</td>
                                    <td class="total">Total</td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${listcart}" var="c">
                                    <tr>
                                        <td class="cart_product">
                                            <input type="checkbox"  name="ServiceID" value="${c.getService().getServiceId()}">
                                        </td>
                                        <td>
                                            <h4><a href="">${c.getService().getServiceId()}</a></h4>
                                        </td>
                                        <td class="cart_description">
                                            <h4><a href="">${c.getService().getServiceName()}</a></h4>
                                        </td>
                                        <td class="cart_price">
                                            <p>${c.getService().getPrice()}</p>
                                        </td>
                                        <td class="cart_quantity">
                                            <div class="cart_quantity_button">
                                                <a class="cart_quantity_up" href="ReservationDetails?upService=${c.getService().getServiceId()}&check=${true}"> + </a>
                                                <input class="cart_quantity_input" type="text" name="quantity" value="${c.getQuantity()}" autocomplete="off" size="2">
                                                <a class="cart_quantity_down" href="ReservationDetails?upService=${c.getService().getServiceId()}&check=${false}"> - </a> 
                                            </div>
                                        </td>
                                        <td class="cart_quantity">
                                            <div class="cart_quantity_button">
                                                <a class="cart_quantity_up" href="ReservationDetails?upNumberPerson=${c.getService().getServiceId()}&check=${true}"> + </a>
                                                <input class="cart_quantity_input" type="text" name="quantity" value="${c.getNumberPerson()}" autocomplete="off" size="2">
                                                <a class="cart_quantity_down" href="ReservationDetails?upNumberPerson=${c.getService().getServiceId()}&check=${false}"> - </a> 
                                            </div>
                                        </td>
                                        <td class="cart_total">
                                            <p class="cart_total_price">${c.getService().getPrice()*c.getNumberPerson()*c.getQuantity()}</p>
                                        </td>
                                        <td class="cart_delete">
                                            <a class="cart_quantity_delete" href="ReservationDetails?servicedelete=${c.getService().getServiceId()}"><i class="fa fa-times"></i></a>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>

                    </div>

                    <h1>Totals: $${totals}</h1>
                    <hr>
                    <div class="cart-total">
                        <p>Total Cost: $${totals}</p>
                    </div>
                    <button class="checkout-button">Check Out</button>

                </form>
            </div>
        </div>
        <%@include file="footer.jsp" %>
        <script>
            // Hàm goBack() sử dụng JavaScript để quay lại trang trước đó trong lịch sử duyệt
            function goBack() {
                window.history.back();
            }
        </script>
    </body>
</html>

