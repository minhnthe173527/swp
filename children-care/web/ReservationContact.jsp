<%-- 
    Document   : ResavationDetails
    Created on : Sep 16, 2023, 11:38:06 PM
    Author     : SANG
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Giỏ Hàng1</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <style>
            /* CSS cho layout chung */
            body {
                font-family: Arial, sans-serif;
                margin: 0;
                padding: 0;
                justify-content: center;
                background-color: #f4f4f4;
            }

            .sider {
                width: 20%;
                padding: 20px;
                background-color: #ece9e9;
                color: #fff;
            }

            .main-content {
                width: 80%;
                padding: 20px;
            }

            /* CSS cho giỏ hàng */
            .cart {
                margin: 20px;
                padding: 20px;
                border: 1px solid #ccc;
                border-radius: 5px;
                background-color: #fff;
            }

            .cart h2 {
                font-size: 1.5rem;
                margin-bottom: 20px;
            }

            .cart-item {
                display: flex;
                margin-bottom: 20px;
            }

            .cart-item img {
                max-width: 100px;
                max-height: 100px;
                margin-right: 20px;
                border-radius: 5px;
            }

            .cart-item-details {
                flex: 1;
            }

            .cart-item h3 {
                margin: 0;
            }

            .cart-item p {
                margin: 5px 0;
            }

            /* CSS cho sidebar */
            .sidebar {
                flex-basis: 22%;
                background-color: #e0f2f1;
                ; /* Set the background color for the sidebar */
                padding: 20px;
            }

            .sidebar h2 {
                font-size: 1.5rem;
                margin-bottom: 10px;
            }

            .category-list {
                list-style: none;
                padding: 0;
            }

            .category-list li {
                margin-bottom: 10px;
            }

            /* CSS cho ô tìm kiếm */
            .search-box {
                margin-top: 20px;
                padding: 20px;
                border: 1px solid #ccc;
                border-radius: 5px;
                background-color: #fff;
            }

            .search-box h2 {
                font-size: 3px;
                margin-bottom: 10px;
            }

            .search-input {
                width: 100%;
                padding: 10px;
                border: 1px solid #ccc;
                border-radius: 5px;
            }

            .search-button {
                background-color: #007bff;
                color: #fff;
                border: none;
                border-radius: 5px;
                padding: 5px 20px;
                cursor: pointer;
            }
            .cart_quantity_button a {
                background:#F0F0E9;
                color: #696763;
                display: inline-block;
                font-size: 20px;
                height: 28px;
                overflow: hidden;
                text-align: center;
                width: 35px;
                float: left;
            }
            .cart_quantity_input {
                color: #696763;
                float: left;
                font-size: 16px;
                text-align: center;
                font-family: 'Roboto',sans-serif;

            }
            .order-summary {
                margin-top: 50px;
            }
            .order-summary table {
                border-collapse: collapse;
                width: 100%;
            }
            .order-summary th, .order-summary td {
                border: 1px solid #ddd;
                padding: 10px;
                text-align: left;
            }
            .order-summary th {
                background-color: #eee;
            }
            .instruction {
                display: flex;
                flex-direction: row;
                align-items: center;
                justify-content: space-around;
                margin-top: 50px;
                background-color: #f2f2f2;
                padding: 20px;
            }
            .instruction > div {
                display: flex;
                flex-direction: column;
                align-items: center;
                text-align: center;
            }
            .instruction h3 {
                font-size: 1.5rem;
                margin-bottom: 10px;
            }
            .instruction p {
                font-size: 1.2rem;
                line-height: 1.5;
            }
            .instruction {
                display: flex;
                flex-direction: row;
                align-items: center;
                justify-content: space-around;
                margin-top: 50px;
                background-color: #f2f2f2;
                padding: 20px;
            }

            .instruction > div {
                flex-basis: 33.33%;
                box-sizing: border-box;
                display: flex;
                flex-direction: column;
                align-items: center;
                text-align: center;
            }

            .instruction h3 {
                font-size: 1.5rem;
                margin-bottom: 10px;
            }

            .instruction p {
                font-size: 1.2rem;
                line-height: 1.5;
            }
        </style>
    </head>
    <body>
        <%@include file="Header.jsp" %>
        <div>
            <button onclick="goBack()"><<-Back</button>
        </div>
        <section>
            <div class="row">
                <div class="col-sm-3">
                    <div class="sidebar">
                        <!-- Add your sidebar content here -->
                        <form action="servicelist">
                            <div class="form-group">
                                <label for="txtSearchValue">Search Value:</label>
                                <input type="text" class="form-control" id="txtSearchValue" name="txtSearchValue" value="${param.txtSearchValue}">
                            </div>
                            <button type="submit" class="btn btn-primary" name="btAction">Search</button>
                            <h4>Service Category:</h4>
                        </form>
                        <ul class="list-group">
                            <li class="list-group-item">
                                <a href="servicelist?txtCateService=Medical%20Check-up" class="btn btn-secondary">Medical Check-up</a>
                            </li>
                            <li class="list-group-item">
                                <a href="servicelist?txtCateService=Medical%20Consultation" class="btn btn-secondary">Medical Consultation</a>
                            </li>
                            <li class="list-group-item">
                                <a href="servicelist?txtCateService=Health%20Care" class="btn btn-secondary">Health Care</a>
                            </li>
                        </ul>
                        <ul>
                            <li><a href="https://www.facebook.com/"><span class="fa fa-facebook"><i>Fan Page :</i></span></a></li>
                            <li>Hotline: <a href="tel:0855809219">0855.809.219 (8h - 22h)</a></li>
                            <li><a href="https://www.google.com/maps/d/u/0/viewer?mid=1qkoM0-zP-XrdOvBa6eVDVSQ4pyY&hl=en_US&ll=21.013391000000013%2C105.5266735&z=17"><span class="fa fa-dribbble"><i>Address :</i></span></a></li>
                        </ul>
                    </div>                </div>
                <form action="" method="">
                    <div class="col-sm-9">
                        <h1>Reservation Contact</h1>
                        <div class="order-summary">
                            <h3>Information line</h3>
                            <table>
                                <tr>
                                    <th>Service ID</th>
                                    <th>Title      </th>
                                    <th>Quantity</th>
                                    <th>Number of person</th>
                                    <th>price</th>
                                    <th>Total Cost</th>
                                </tr>
                                <c:forEach items="${listCart}" var="c" >
                                    <tr>
                                        <td><input type="text" value="${c.getService().getServiceId()}" readonly=""></td>
                                        <td><input type="text" value="${c.getService().getDescription()}" readonly=""></td>
                                        <td>${c.getQuantity()}</td>
                                        <td>${c.getNumberPerson()}</td>
                                        <td>${c.getService().getPrice()}</td>
                                        <td>${c.getService().getPrice()*c.getQuantity()*c.getNumberPerson()}</td>
                                    </tr>
                                </c:forEach>
                            </table>
                            <p></p>
                            <p>---------------------------------------------------------------------------------------------</p>
                        </div>
                        <div class="cart-total">
                            <p>Total cost: $${total}</p>
                        </div>
                        <table>
                            <tr>
                            <div class="form-group">
                                <td> <label for="name">Name:</label></td>
                                <td> <input style="width: 300px" type="text" id="name" name="name" value="${sessionScope.account.getFullname()}"></td>
                            </div>
                            </tr>
                            <tr>
                            <div class="form-group">
                                <td><label for="email">Email:</label></td>
                                <td><input style="width: 300px" type="email" id="email" name="email" required="" value="${sessionScope.account.getEmail()}"></td>
                            </div>
                            </tr>
                            <tr>
                            <div class="form-group">
                                <td><label for="phone">Phone:</label></td>
                                <td><input style="width: 300px" type="tel" id="phone" name="phone" required="" value="${sessionScope.account.getMobile()}"></td>
                            </div>
                            </tr>
                            <tr>
                            <div class="form-group">
                                <td><label for="phone">Address:</label></td>
                                <td><input style="width: 300px" type="tel" name="address" value="${sessionScope.account.getAddress()}"></td>
                            </div>
                            </tr>
                            <tr>
                            <div class="form-group">
                                <td><label for="phone">Gender:</label></td>    
                                <td><select name="gender" id="gender">
                                        <option <c:if test="${sessionScope.account.isGender()==false}">selected</c:if> value="0">Female</option>
                                        <option <c:if test="${sessionScope.account.isGender()}">selected</c:if> value="1">Male</option>
                                        </select></td>
                                </div>
                                </tr>
                            </table>
                            <button class="checkout-button">Submit</button>
                        </div>
                    </form>
                </div>
            </section>
        <%@include file="footer.jsp" %>
        <script>
            // Hàm goBack() sử dụng JavaScript để quay lại trang trước đó trong lịch sử duyệt
            function goBack() {
                window.history.back();
            }
        </script>
    </body>
</html>

