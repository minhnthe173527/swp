/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controllers.Common;

import DAO.AccountDAO;
import Model.Account;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.Base64;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;

/**
 *
 * @author Lenovo
 */
public class RegisterServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            response.setContentType("text/html;charset=UTF-8");
            //forward to Register.jsp for entering informations
            String email = request.getParameter("email");
            if (email == null) {
                request.getRequestDispatcher("Register.jsp").forward(request, response);
                return;
            }
            String password = request.getParameter("password");
            //check password format
            String regex = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$";
            if(!password.matches(regex)) {
                request.setAttribute("msg", "Passwords must have at least 8 characters and contain at least one uppercase letters, lowercase letters, numbers, or symbols.");
                request.getRequestDispatcher("Register.jsp").forward(request, response);
                return;
            }
            String fname = request.getParameter("fname");
            String lname = request.getParameter("lname");
            String gender = request.getParameter("gender");
            String mobile = request.getParameter("mobile");
            String address = request.getParameter("address");
            AccountDAO dao = new AccountDAO();
            //check email is exist in database
            if (dao.checkExist(email)) {
                request.setAttribute("msg", "Your email is already exist");
                request.getRequestDispatcher("Register.jsp").forward(request, response);
                return;
            }
            //send verify link to user account
            long duration = 300; //link expire in 5m
            long expirationTime = new Date().getTime() / 1000 + duration; //set expired time
            String data = email + "|" + fname + " " + lname + "|" + password + "|" + gender + "|" + mobile + "|" + address + "|" + expirationTime;//data for encode
            String encodedData = Base64.getEncoder().encodeToString(data.getBytes()); //encode data
            String url = "http://localhost:9999/ChildrenCare/signupverify?data=" + encodedData;//create url
            //set up content
            String content = "<html><body>";
            content += "<p>Click here to verify</p>";
            content += "<a href='" + url + "'><button style='background-color: Blue; color: white; padding: 10px 20px; border: none; cursor: pointer;'>Verify</button></a>";
            content += "<p>Your url will be expired in 5 minutes</p>";
            content += "</body></html>";
            //send email
            dao.sendEmail(email, content);
            PrintWriter out = response.getWriter();
            out.println("<script>");
            out.println("var confirmation = confirm('We have sent you an email to verify your account, check email?')");
            out.println("if (confirmation) {");
            out.println("window.location.href = 'https://www.gmail.com';");
            out.println("} else {");
            out.println("window.location.href = 'login';");
            out.println("}");
            out.println("</script>");
        } catch (MessagingException ex) {
            Logger.getLogger(RegisterServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
