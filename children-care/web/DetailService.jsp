<%-- 
    Document   : detail
    Created on : Sep 19, 2023, 11:29:53 PM
    Author     : minh1
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="css/style.css">
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
        <%@include file="Header.jsp" %>
        <div>
            <button onclick="goBack()"><<-Back</button>
        </div>
        <main>
        <div class="left-div">
            <form action="searchservicebyname"method ="get">
                Search Value: <input type="text" name="search"/>          
                <input type="submit" value="Search" name="btAction" />
            </form><br/>
            <p style="font-size: 24px; font-weight: bold ">Service category :</p>
            <form action="searchservicebycategory" method="get">
                <c:forEach items="${requestScope.serc}" var = "sc">
                    <input type="submit" name="searchbyccname" value="${sc.cname}" class="btn-link"/><br/>
                </c:forEach>
            </form>
            
            <ul>
                <li><a href="#"><span class="fa fa-facebook"><i>Fan Page :</i></span></a></li>
                <li>Hotline: <a href="tel:0855809219">0855.809.219 (8h - 22h)</a></li>
                <li><a href="#"><span class="fa fa-dribbble"><i>Address :</i></span></a></li>
            </ul>

        </div>

        <div class="right-div">
            <c:set var="s" value="${requestScope.ser}" /> 
            <div class="container mt-4">
                <div class="service-card">
                    <div class="service-img">
                        <img src="images/${s.image}" alt="Image" />
                    </div>
                    <div class="service-details">
                        <h2 class="name-content">${s.serviceName}</h2>
                        <h4 class="description">${s.description}</h4>
                        <h3 class="price">Price: ${s.price} $</h3>
                    </div>
                </div>
                    
                <form action="ReservationDetails" method="get"> 
                    <button  class="btn-17">
                        <input type="text" hidden name="id" value="${s.serviceId}">
                        <span class="text-container">
                            <span class="text">Check Out</span>
                        </span>
                    </button> 
                </form>
            </div>
        </div>
                    </main>
        <div class="clearfix"></div>
        <style>
            body {
                height: 100%;
                margin: 0;
                padding: 0;
            }
            .left-div {
                width: 26%;
                height : 80%;
                float: left; /* Xếp cạnh trái */
                background-color: #e0f2f1; /* Màu nền xanh lam nhạt */
                margin: 1%; /* Khoảng cách xung quanh div */
                padding: 1%; /* Khoảng cách bên trong div */

            }

            .btn-link {
            width: 100%; /* Chiều rộng 100% của phần tử cha */
            margin-bottom: 5px; /* Khoảng cách giữa các nút */
        }
            
            
            .right-div {
                width: 70%; /* Chiếm phần còn lại bên phải */
                float: left; /* Xếp cạnh trái */
            }

            /* Xóa float và thiết lập khoảng cách giữa các nhóm div */
            .clearfix::after {
                content: "";
                clear: both;
                display: table;
            }





            /* phần nút bấm */
            .btn-17,
            .btn-17 *,
            .btn-17 :after,
            .btn-17 :before,
            .btn-17:after,
            .btn-17:before {
                border: 0 solid;
                box-sizing: border-box;
            }

            .btn-17 {
                -webkit-tap-highlight-color: transparent;
                -webkit-appearance: button;
                background-color: #000;
                background-image: none;
                color: #fff;
                cursor: pointer;
                font-family: ui-sans-serif, system-ui, -apple-system, BlinkMacSystemFont,
                    Segoe UI, Roboto, Helvetica Neue, Arial, Noto Sans, sans-serif,
                    Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol, Noto Color Emoji;
                font-size: 100%;
                font-weight: 900;
                line-height: 1.5;
                margin: 0;
                -webkit-mask-image: -webkit-radial-gradient(#000, #fff);
                padding: 0;
                text-transform: uppercase;

            }

            .btn-17:disabled {
                cursor: default;
            }

            .btn-17:-moz-focusring {
                outline: auto;
            }

            .btn-17 svg {
                display: block;
                vertical-align: middle;
            }

            .btn-17 [hidden] {
                display: none;
            }

            .btn-17 {
                border-radius: 99rem;
                border-width: 2px;
                padding: 0.8rem 3rem;
                z-index: 0;
            }

            .btn-17,
            .btn-17 .text-container {
                overflow: hidden;
                position: relative;
            }

            .btn-17 .text-container {
                display: block;
                mix-blend-mode: difference;
            }

            .btn-17 .text {
                display: block;
                position: relative;
            }

            .btn-17:hover .text {
                -webkit-animation: move-up-alternate 0.3s forwards;
                animation: move-up-alternate 0.3s forwards;
            }

            @-webkit-keyframes move-up-alternate {
                0% {
                    transform: translateY(0);
                }

                50% {
                    transform: translateY(80%);
                }

                51% {
                    transform: translateY(-80%);
                }

                to {
                    transform: translateY(0);
                }
            }

            @keyframes move-up-alternate {
                0% {
                    transform: translateY(0);
                }

                50% {
                    transform: translateY(80%);
                }

                51% {
                    transform: translateY(-80%);
                }

                to {
                    transform: translateY(0);
                }
            }

            .btn-17:after,
            .btn-17:before {
                --skew: 0.2;
                background: #fff;
                content: "";
                display: block;
                height: 102%;
                left: calc(-50% - 50% * var(--skew));
                pointer-events: none;
                position: absolute;
                top: -104%;
                transform: skew(calc(150deg * var(--skew))) translateY(var(--progress, 0));
                transition: transform 0.2s ease;
                width: 100%;
            }

            .btn-17:after {
                --progress: 0%;
                left: calc(50% + 50% * var(--skew));
                top: 102%;
                z-index: -1;
            }

            .btn-17:hover:before {
                --progress: 100%;
            }

            .btn-17:hover:after {
                --progress: -102%;
            }

        </style>

        <%@include file="footer.jsp" %>    
        <script>
        // Hàm goBack() sử dụng JavaScript để quay lại trang trước đó trong lịch sử duyệt
        function goBack() {
            window.history.back();
        }
    </script>
    </body>
</html>
