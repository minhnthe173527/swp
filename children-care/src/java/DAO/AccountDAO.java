package DAO;

import Model.Account;
import Model.ManagerAcc;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class AccountDAO {

    public static void main(String[] args) throws MessagingException {
        AccountDAO dao = new AccountDAO();
        System.out.println(dao.updateProfile("Chinh", "vichinh230@gmail.com", "0123456789", true, "UserAccount"));
    }

    public boolean checkUnauthorizationPage(String url) {
        try {
            Connection con = new DBContext().connection;
            String sql = "select* from Unauthorize where url like ?";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setString(1, "%" + url + "%");
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                return true;
            }

        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean updateProfile(String name, String email, String phone, boolean gender, String table) {
        try {
            String sql = "update " + table + " SET Name = ?, Mobile = ?, Gender = ? WHERE Email = ? ";
            Connection con = new DBContext().connection;
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, name);
            ps.setString(2, phone);
            ps.setBoolean(3, gender);
            ps.setString(4, email);
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {

        }
        return false;
    }

    public boolean checkPermission(String role, String url) {
        try {
            Connection con = new DBContext().connection;
            String sql = "select* from Authorize where role like ? and url like ?";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setString(1, "%" + role + "%");
            stm.setString(2, "%" + url + "%");
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                return true;
            }

        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public Account getAccount(Account account) {
        Account ac = checkUserAccount(account, "UserAccount");
        if (ac != null) {
            return ac;
        }
        ac = checkUserAccount(account, "EmployeeAccount");
        if (ac != null) {
            return ac;
        }

        return null;
    }

    public boolean userRegister(Account account) {
        try {
            Connection con = new DBContext().connection;
            String sql = "insert into UserAccount values (?,?,?,?,?,?,?,?)";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setInt(1, getUserID());
            stm.setString(2, account.getFullname());
            stm.setString(3, account.getEmail());
            stm.setString(4, account.getPassword());
            stm.setString(5, account.getMobile());
            stm.setBoolean(6, account.isGender());
            stm.setBoolean(7, false);
            stm.setString(8, account.getAddress());
            stm.execute();
            return true;

        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public int getUserID() {
        int id = 0;
        try {
            Connection con = new DBContext().connection;
            String sql = "select ID from UserAccount";
            PreparedStatement stm = con.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                id = rs.getInt(1);
            }
            return id + 1;
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public boolean checkExist(String email) {

        if (findAccount(email, "UserAccount")) {
            return true;
        }
        return findAccount(email, "EmployeeAccount");
    }

    public boolean findAccount(String email, String table) {
        try {
            Connection con = new DBContext().connection;
            String sql = "select*from " + table + " where email like ?";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                return true;
            }

        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public Account checkLogin(Account account) {
        Account ac = checkUserAccount(account, "UserAccount");
        if (ac != null) {
            return ac;
        }
        ac = checkUserAccount(account, "EmployeeAccount");
        if (ac != null) {
            return ac;
        }
        return null;
    }

    public Account checkUserAccount(Account account, String table) {
        try {
            Connection con = new DBContext().connection;
            String sql = "select * from " + table + " where email like ? and password like ?";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setString(1, account.getEmail());
            stm.setString(2, account.getPassword());
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                if ("UserAccount".equals(table)) {
                    return new Account(rs.getInt("id"), rs.getString("name"), rs.getString("email"), rs.getString("password"), rs.getString("mobile"), rs.getBoolean("gender"), "customer", rs.getString("address"), rs.getBoolean("status"));
                } else {
                    return new Account(rs.getInt("id"), rs.getString("name"), rs.getString("email"), rs.getString("password"), rs.getString("mobile"), rs.getBoolean("gender"), rs.getString("role"), rs.getString("address"), rs.getBoolean("status"));
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void sendEmail(String recipentEmail, String content) throws MessagingException {
        //connect smtp server
        Properties prop = new Properties();
        prop.setProperty("mail.smtp.auth", "true");
        prop.setProperty("mail.smtp.starttls.enable", "true");
        prop.setProperty("mail.smtp.host", "smtp.gmail.com");
        prop.setProperty("mail.smtp.port", "587");
        //
        Session session = Session.getInstance(prop, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                String username = "emailtest33781@gmail.com";
                String password = "wfwsjzkvupmnipke";
                return new PasswordAuthentication(username, password);
            }
        });

        //create message
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("emailtest33781@gmail.com"));
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(recipentEmail));
            message.setSubject("Verify");
            message.setText(content);
            message.setContent(content, "text/html; charset=utf-8");
            Transport.send(message);

            System.out.println("Email đã gửi thành công!");
        } catch (MessagingException e) {
            e.printStackTrace();
            System.err.println("Lỗi khi gửi email: " + e.getMessage());
        }

    }

    public String checkPassword(String email) {
        String password;
        try {
            Connection con = new DBContext().connection;
            String sql = "select password from UserAccount where email like ?";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                password = rs.getString(1);
                return password;
            }

        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public boolean changePassword(String email, String newPassword, String table) {
        try {
            Connection con = new DBContext().connection;
            String sql = "update " + table + " set password = ? where email like ?";
            PreparedStatement stm = con.prepareStatement(sql);
            stm.setString(1, newPassword);
            stm.setString(2, email);
            String currentPassword = checkPassword(email);
            if (newPassword.equals(currentPassword)) {
                System.out.println("new password matches current password");
            } else {
                stm.execute();
                return true;
            }

        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public ArrayList<ManagerAcc> getAccount(int index) {
        ArrayList<ManagerAcc> list = new ArrayList<>();
        String sql = "SELECT * FROM [EmployeeAccount] order by id "
                + " OFFSET ? ROWS FETCH NEXT 9 ROWS ONLY";
        try {
            Connection con = new DBContext().connection;
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, (index - 1) * 6);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new ManagerAcc(rs.getInt(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getBoolean(6), rs.getString(7), rs.getString(8), rs.getString(9)));
            }
        } catch (SQLException e) {

        }
        return list;
    }

    public int getNumberAccount() {
        ArrayList<Account> list = new ArrayList<>();
        String sql = "SELECT count(*) FROM [EmployeeAccount]";
        try {
            Connection con = new DBContext().connection;
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException e) {
        }
        return 0;
    }

    public ManagerAcc getAccountById(int id) {
        try {
            String sql = "SELECT * FROM [EmployeeAccount] where id = ? ";
            Connection con = new DBContext().connection;
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ManagerAcc acc = new ManagerAcc(rs.getInt(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getBoolean(6), rs.getString(7), rs.getString(8), rs.getString(9));
                return acc;
            }
        } catch (SQLException e) {

        }
        return null;
    }

    public ArrayList<ManagerAcc> getSortedEmployeeAccounts(String sortBy, int index) {
        ArrayList<ManagerAcc> list = new ArrayList<>();
        String query = "SELECT * FROM [EmployeeAccount] ORDER BY ";
        if ("1".equals(sortBy)) {
            query += "id";
        } else if ("2".equals(sortBy)) {
            query += "id desc";
        } else if ("3".equals(sortBy)) {
            query += "role";
        } else if ("4".equals(sortBy)) {
            query += "status";
        } else if ("5".equals(sortBy)) {
            query += "gender";
        }
        query += " OFFSET ? ROWS FETCH NEXT 9 ROWS ONLY";

        try {
            Connection con = new DBContext().connection;
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1, (index - 1) * 6);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new ManagerAcc(rs.getInt(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getBoolean(6), rs.getString(7), rs.getString(8), rs.getString(9)));
            }
        } catch (SQLException e) {
        }

        return list;
    }

    public ArrayList<ManagerAcc> searchAccount(String search, int index) {
        ArrayList<ManagerAcc> list = new ArrayList<>();
        String sql = "SELECT * FROM [EmployeeAccount] where [Name] like ? OR email like ? OR mobile like ? "
                + "order by [Name], email OFFSET ? ROWS FETCH NEXT 9 ROWS ONLY ";
        try {
            Connection con = new DBContext().connection;
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, "%" + search + "%");
            ps.setString(2, "%" + search + "%");
            ps.setString(3, "%" + search + "%");
            ps.setInt(4, (index - 1) * 6);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new ManagerAcc(rs.getInt(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getBoolean(6), rs.getString(7), rs.getString(8), rs.getString(9)));
            }
        } catch (SQLException e) {

        }
        return list;
    }
    
}
