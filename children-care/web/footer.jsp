<%-- 
    Document   : footer
    Created on : Sep 21, 2023, 2:25:02 PM
    Author     : hocsi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <style>
        .footer {
            display: flex;
            justify-content: space-between;
            align-items: center;
            background-color: #000000;
            padding: 32px;
            color: white;
            margin-top: 40px;
        }

        .left-content {
            flex: 1;
            text-align: right;
            padding-right: 16px;
        }

        .right-content {
            flex: 1;
            text-align: left;
            padding-left:  16px;
        }

        .contact-details {
            display: flex;
            align-items: center;
        }

        .contact-details img {
            max-width: 20px;
            height: auto;
            margin-right: 10px;
        }
    </style>
</head>
<body>
    <!-- Your page content here -->

    <div class="footer">
        <div class="left-content">
            <p>Project SWP
            Children-care
            </p>
        </div>
        <div class="right-content">
            <div class="contact">
                Adress: Hòa Lạc, Thạch Thất , Hà Nội<br>
                Hotline: 0855.809.219<br>
                Email: minhdthe163918@fpt.edu.vn
            </div>
            <div class="social-media">
                <a href="https://www.messenger.com/t/100045191402704" target="_blank">
                    <img src="images/messenger.svg" style="max-width: 220px;"alt="Messenger">
                </a>
            </div>
        </div>
    </div>
</body>
</html>
