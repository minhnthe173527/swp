/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import java.util.Date;


/**
 *
 * @author ADMIN
 */

public class Feedback {
    private int feedbackId;
    private int customerId;
    private String content;
    private Date date;
    private int serviceId;
    public String img;
    public float rate;
    public boolean status;
    private Service service;
    private CustomerAcc customerAcc;
    private String PublishedDate;

    public Feedback() {
    }

    public Feedback(int feedbackId, int customerId, String content, Date date, int serviceId) {
        this.feedbackId = feedbackId;
        this.customerId = customerId;
        this.content = content;
        this.date = date;
        this.serviceId = serviceId;
    }

    public String getPublishedDate() {
        return PublishedDate;
    }

    public void setPublishedDate(String PublishedDate) {
        this.PublishedDate = PublishedDate;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public CustomerAcc getCustomerAcc() {
        return customerAcc;
    }

    public void setCustomerAcc(CustomerAcc customerAcc) {
        this.customerAcc = customerAcc;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public int getFeedbackId() {
        return feedbackId;
    }

    public void setFeedbackId(int feedbackId) {
        this.feedbackId = feedbackId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getServiceId() {
        return serviceId;
    }

    public void setServiceId(int serviceId) {
        this.serviceId = serviceId;
    }
    
}