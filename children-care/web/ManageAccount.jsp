<%-- 
    Document   : manageUser
    Created on : Sep 26, 2023, 11:57:11 AM
    Author     : asus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="./marketing/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="./marketing/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="./marketing/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link href="./marketing/css/morris/morris.css" rel="stylesheet" type="text/css" />
        <link href="./marketing/css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
        <link href="./marketing/css/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
        <link href="./marketing/css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
        <link href="./marketing/css/iCheck/all.css" rel="stylesheet" type="text/css" />
        <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
        <!-- Theme style -->
        <link href="./marketing/css/style.css" rel="stylesheet" type="text/css" />

        <style type="text/css">
            td{
                padding: 5px;
            }
            th{
                padding: 5px;
            }
        </style>
        <script src="ckeditor/ckeditor.js"></script> 
        <script src="ckfinder/ckfinder.js"></script>

        <!-- Boxicons -->
        <link href='https://unpkg.com/boxicons@2.0.9/css/boxicons.min.css' rel='stylesheet'>
        <!-- My CSS -->
        <link rel="stylesheet" href="admin/style.css">

        <title>Manage Account</title>
    </head>
    <body class="skin-black">
         <c:if test="${param['index']==null }">   
            <c:set var = "index" scope = "page" value = "1"/>
        </c:if>
        <c:if test="${param['index']!=null}">
            <c:set var = "index" scope = "page" value = "${param['index']}"/>
        </c:if>
        <div class="left-side sidebar-offcanvas"  style="height: 1200px;position: fixed; width: 220px; background-color: #39435c;left: 0; top:0;">
        </div>
        <header class="header">
            <a href="" class="logo">
                Admin
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <a href="logout" class="btn btn-outline-primary" style="text-decoration: none;  font-weight: 100;" >
                        <i class="fa fa-sign-out" style="font-size: 35px;" aria-hidden="true"></i>
                    </a>
                </div>
            </nav>
        </header>
        <!-- SIDEBAR -->
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas" id="left-aside" style="height: 10%;">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <c:if test="${sessionScope.u.getAvartar()==null}">
                                <img src="https://dvdn247.net/wp-content/uploads/2020/07/avatar-mac-dinh-1.png" class="img-circle" alt="User Image" />
                            </c:if>
                            <c:if test="${sessionScope.u.getAvartar()!=null}">
                                <img src="${sessionScope.u.getAvartar()}" class="img-circle" alt="User Image" />
                            </c:if>
                        </div>
                        <div class="pull-left info">
                            <p>Hello, ${sessionScope.u.getFullname()}</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu"> 
                        
                        <li class="">
                            <a href="AdminSetting">
                                <i class="fa fa-circle"></i> <span>Setting List</span>
                            </a>
                        </li>
                       
                        <li class="">
                            <a href="ManageAccount">
                                <i class="fa fa-circle"></i> <span>Manager Account</span>
                            </a>
                        </li>
                        
                        <li class="">
                            <a href="home">
                                <i class="fa fa-circle"></i> <span>Go Home</span>
                            </a>
                        </li>
                    </ul>
                </section>
                <!-- SIDEBAR --></aside>
        <!-- SIDEBAR -->



        <!-- CONTENT -->
        <section id="content">
            <!-- NAVBAR -->
            <nav>
                
                <form action="SearchAccount" method="post">
                    <div class="form-input">
                        <input type="search" name="search" placeholder="Search...">
                        <button type="submit" class="search-btn"><i class='bx bx-search' ></i></button>
                    </div>
                </form>
                
            </nav>
            <!-- NAVBAR -->

            <!-- MAIN -->
            <main>
                <div class="head-title">
                    <div class="left">
                        <h1>Manage Account</h1>
                    </div>
                    
                </div>

                <div class="table-data">
                    <div class="order">
                        <div class="head">
                            <h3>Account List</h3>
                            <form action="ManageAccount" method="post">
                                <select style="
                                        height: 29px;
                                        width: 9rem;
                                        border-radius: 10px;" name="sort" onchange="this.form.submit()">
                                    <option value="0">Filter</option>
                                    <option value="1">Sort By Id Ascending</option>
                                    <option value="2">Sort By Id Descending </option>
                                    <option value="3">Sort By Role</option>
                                    <option value="4">Sort By Status</option>
                                    <option value="5">Sort By Gender</option>
                                </select>
                            </form>
                        </div>

                        <table>
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Full Name</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th>Gender</th>
                                    <th>Adress</th>
                                    <th>Role</th>
                                    <th>Status</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${account}" var="a">
                                    <tr>
                                        <td><a style="color: black" href="AccountDetail?id=${a.getManagerId()}">${a.getManagerId()}</a></td>
                                        <td><a style="color: black" href="AccountDetail?id=${a.getManagerId()}">${a.getFullname()}</a></td>
                                        <td><a style="color: black" href="AccountDetail?id=${a.getManagerId()}">${a.getEmail()}</a></td>
                                        <td><a style="color: black" href="AccountDetail?id=${a.getManagerId()}">${a.getMobile()}</a></td>
                                        <td><a style="color: black" href="AccountDetail?id=${a.getManagerId()}">${a.isGender()?'Male':'Female'}</a></td>
                                        <td><a style="color: black" href="AccountDetail?id=${a.getManagerId()}">${a.getAddress()}</a></td>
                                        <td><a style="color: black" href="AccountDetail?id=${a.getManagerId()}">${a.getRole()}</a></td>
                                        <td><a style="color: black" href="AccountDetail?id=${a.getManagerId()}">
                                                <span class="status completed">
                                                    ${a.getStatus()}</span></a></td>
                                        <td><a href="DeleteUser?uid=${u.getUserId()}" onclick="return confirm('Bạn có chắc chắn muốn xóa ${u.getFulName()} không?')" >
                                                <span style="font-size: 18px;" class="status pending"><i class='bx bx-trash' ></i></span></a></td>
                                    </tr>
                                </c:forEach>               
                            </tbody>
                        </table>
                        <div class="clearfix">
                            <ul class="pagination">
                                <li class="page-item disabled"><a href="ManageAccount?index=1">Previous</a></li>
                                    <c:forEach var = "i" begin = "1" end = "${numberPage}">
                                    <li class="${param['index']==i?'page-item active':''}"><a class="page-link" href="ManageAccount?index=${i}"><span>${i}</span></a></li>
                                            </c:forEach>
                                <li class="page-item"><a href="ManageAccount?index=${numberPage}" class="page-link">Next</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </main>
            <!-- MAIN -->
        </section>
        <!-- CONTENT -->


        <script src="admin/script.js"></script>
    </body>
</html>
