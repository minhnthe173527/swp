/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;

import Model.CustomerAcc;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author SANG
 */
public class CustomerAccountDAO {

    public CustomerAcc getCustomerAcc(int id) {
        Connection con = new DBContext().connection;
        try {
            String sql = "  select * from [UserAccount] c where c.ID = " + id;
            PreparedStatement stm = con.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                /*
                [CustomerID]
                ,[FullName]
                ,[Email]
                ,[Password]
                ,[Mobile]
                ,[Gender]
                ,[Status]
                ,[Address]
                 */
                CustomerAcc acc = new CustomerAcc();
                acc.setCustomerId(rs.getInt(1));
                acc.setFullname(rs.getString(2));
                acc.setEmail(rs.getString(3));
                acc.setPassword(rs.getString(4));
                acc.setMobile(rs.getString(5));
                int gender = rs.getInt(6);
                if (gender == 1) {
                    acc.setGender(true);
                } else {
                    acc.setGender(false);
                }
                acc.setStatus(rs.getString(7));
                acc.setAddress(rs.getString(8));
                return acc;
            }
        } catch (Exception e) {
        }
        return null;
    }
}
