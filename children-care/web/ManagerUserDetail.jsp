<%-- 
    Document   : ManagerUserDetail
    Created on : Oct 14, 2023, 1:11:25 PM
    Author     : minh1
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="./marketing/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="./marketing/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="./marketing/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link href="./marketing/css/morris/morris.css" rel="stylesheet" type="text/css" />
        <link href="./marketing/css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
        <link href="./marketing/css/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
        <link href="./marketing/css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
        <link href="./marketing/css/iCheck/all.css" rel="stylesheet" type="text/css" />
        <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
        <!-- Theme style -->
        <link href="./marketing/css/style.css" rel="stylesheet" type="text/css" />

        <style type="text/css">
            td{
                padding: 5px;
            }
            th{
                padding: 5px;
            }
        </style>
    </head>
    <body class="skin-black">
        <c:if test="${param['index']==null }">   
            <c:set var = "index" scope = "page" value = "1"/>
        </c:if>
        <c:if test="${param['index']!=null}">
            <c:set var = "index" scope = "page" value = "${param['index']}"/>
        </c:if>
        <!-- header logo: style can be found in header.less -->
        <div class="left-side sidebar-offcanvas"  style="height: 1200px;position: fixed; width: 220px; background-color: #39435c;left: 0; top:0;">
        </div>
        <header class="header">
            <a href="" class="logo">
                Manager
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <a href="logout" class="btn btn-outline-primary" style="text-decoration: none;  font-weight: 100;" >
                        <i class="fa fa-sign-out" style="font-size: 35px;" aria-hidden="true"></i>
                    </a>
                </div>
            </nav>
        </header>


        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas" id="left-aside" style="height: 100%;">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <c:if test="${sessionScope.u.getAvartar()==null}">
                                <img src="https://dvdn247.net/wp-content/uploads/2020/07/avatar-mac-dinh-1.png" class="img-circle" alt="User Image" />
                            </c:if>
                            <c:if test="${sessionScope.u.getAvartar()!=null}">
                                <img src="${sessionScope.u.getAvartar()}" class="img-circle" alt="User Image" />
                            </c:if>
                        </div>
                        <div class="pull-left info">
                            <p>Hello, ${sessionScope.u.getFullname()}</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu"> 
                        <li class="">
                            <a href="servicelist">
                                <i class="fa fa-circle"></i> <span>Service List</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="FeedbackList">
                                <i class="fa fa-circle"></i> <span>Feedback List</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="managerslider">
                                <i class="fa fa-circle"></i> <span>Sliders List</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="ManagePost">
                                <i class="fa fa-circle"></i> <span>Post List</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="manageruser">
                                <i class="fa fa-circle"></i> <span>Customer List</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="home">
                                <i class="fa fa-circle"></i> <span>Go Home</span>
                            </a>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>
                            
            <aside class="right-side">

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="panel">
                                <header class="panel-heading">
                                    Customer Detail
                                </header>                
                                <div class="panel-body table-responsive">
                                    <div class="sliderList">
                                        <div class="input-group" style="margin-bottom: 10px;width: 100%; ">
                                            <div class="search-slider"
                                                 style="width: 100%; display: flex; justify-content: space-between; position: relative;">

                                                <div class="left-search"></div>
                                                <div class="right-search">

                                                </div>    
                                            </div>
                                        </div>

                                        <div style="margin: 20px 0px" class="row">
                                            <div class="col-sm-8">
                                                <c:set var="u" value="${requestScope.user}"/>
                                                <form method="get" action="managerchangeprofileuser" >
                                                    <label class="nameprofile">Name</label><input style="text-align: center" required class="nameprofile1" type="text" value="${u.name}" name="name"><br/>
                                                    <label class="emailprofile">Email</label><input style="text-align: center" readonly class="emailprofile1" value="${u.email}" name="email"><br/>
                                                    <label class="phoneprofile">Phone Number</label><input style="text-align: center" class="phoneprofile1" value="${u.mobile}" name="phone" pattern="[0-9]{10}" required title="Please enter a 10-digit phone number (e.g., 0123456789)" ><br/>

                                                    <label class="phoneprofile">Password</label><input style="text-align: center" required class="phoneprofile1" value="${u.password}" name="password"><br/>
                                                    <label class="nameprofile">Address</label><input style="text-align: center" required class="nameprofile" value="${u.address}" name="address"><br/>
                                                    <label class="phoneprofile">Gender</label>
                                                    <input type="radio" name="gender" value="1"  ${u.gender ?"Checked":""}> Male  
                                                    <input type="radio" name="gender" value="0"  ${!u.gender ?"Checked":""}> Female
                                                    <input value="${u.id}" name="id" type="hidden">
                                                    <p style="color: greenyellow">${mess}</p>
                                                    <input style="background: #c49b63; color: white" class="editprofile" type="submit" value="Edit"> 
                                                </form>
                                            </div>
                                                    <div class="col-sm-4">
                                                <h6 style="font-size: 24px; font-weight: bold " >History</h6> 
                                                <c:set var="prevName" value="" />
                                                <c:set var="prevMobile" value="" />
                                                <c:set var="prevPassword" value="" />
                                                <c:set var="prevGender" value="" />
                                                <c:set var="prevModifiedDate" value="" />

                                                <c:forEach items="${requestScope.his}" var="h">
                                                    <c:if test="${not empty prevName && h.name ne prevName}">
                                                        ${h.name}
                                                    </c:if>

                                                    <c:if test="${not empty prevMobile && h.mobile ne prevMobile}">
                                                        ${h.mobile}
                                                    </c:if>

                                                    <c:if test="${not empty prevPassword && h.password ne prevPassword}">
                                                        ${h.password}
                                                    </c:if>

                                                    <c:if test="${not empty prevGender && h.gender ne prevGender}">
                                                        ${h.gender}
                                                    </c:if>

                                                    <c:if test="${not empty prevModifiedDate && h.modifiedDate ne prevModifiedDate}">
                                                        ${h.modifiedDate}
                                                    </c:if>

                                                    <c:set var="prevName" value="${h.name}" />
                                                    <c:set var="prevMobile" value="${h.mobile}" />
                                                    <c:set var="prevPassword" value="${h.password}" />
                                                    <c:set var="prevGender" value="${h.gender}" />
                                                    <c:set var="prevModifiedDate" value="${h.modifiedDate}" />
                                                </c:forEach>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>
            </aside>
        </div>
    </body>

    <style>

        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
        }

        /* Add some styling to the header */
        header {
            background-color: #333;
            color: #fff;
            padding: 10px;
            text-align: center;
        }

        /* Add some styling to the form container */
        .ipform {
            margin: 100px auto;
            max-width: 600px;
            padding: 20px;
            border: 1px solid #ccc;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }

        /* Style the form elements */
        form {
            display: flex;
            flex-direction: column;
        }

        h3 {
            text-align: center;
            margin-bottom: 20px;
        }

        label {
            font-weight: bold;
            margin-bottom: 5px;
        }

        input[type="text"],
        input[type="email"] {
            padding: 8px;
            border: 1px solid #ccc;
            border-radius: 4px;
            text-align: center;
        }

        .change {
            color: #fff;
            font-size: 18px;
            font-weight: normal; /* Adjust the font weight as needed */
            text-decoration: none;
            margin-left: 350px;
        }

        .change:hover {
            text-decoration: underline;
        }


        .editprofile {
            padding: 10px 20px;
            background-color: #007bff;
            color: #fff;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        .editprofile:hover {
            background-color: #0056b3;
        }

        p {
            color: #d50000; /* Red color for error messages */
            margin-bottom: 10px;
        }

        /* Add some styling to the footer */
        footer {
            background-color: #333;
            color: #fff;
            padding: 10px;
            text-align: center;
        }

    </style>
</html>
